# CBS Report Server

CBS Report Server is a Java web application for distribute report in PDF format to CBS branch. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Java JDK and Java web container, e.g. Apache Tomcat

## Authors

* **Kittiphat Patanathaworn** - *Initial work* - [https://gitlab.com/kittiphat](https://gitlab.com/kittiphat)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the XYZ License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
