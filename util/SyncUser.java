import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;

// Revision History
//
// 29-11-2005 add hubcode by rawee    
//
// 24-03-2006 add brgrp by rawee          
//
// 02-03-2012 Modified by Tueng+
//            - add try/catch block while fetching data from Profile and updating to MySQL
//              in order to protect program termination by invalid data value

public class SyncUser
{
  private static String profile_dbDriver = "sanchez.jdbc.driver.ScDriver";
  private static String mysql_dbDriver   = "com.mysql.jdbc.Driver";

  private static String readPreference(Statement stmt, String varname)
    throws SQLException
  {
    ResultSet rs    = null;
    String varvalue = null;

    try
    {
      rs = stmt.executeQuery("select varvalue from preferences where varname = '" + varname + "'");
      if (rs.next())
        varvalue = rs.getString("varvalue");
      else
        throw new SQLException("No preference variable: " + varname);
    }
    finally
    {
      if (rs != null) try { rs.close(); rs = null; } catch (Exception e) {}
    }

    return varvalue;
  }

  public static void main(String args[])
    throws ClassNotFoundException, SQLException, UnsupportedEncodingException
  {
    // PROFILE database objects
    Connection profile_conn = null;
    Statement profile_stmt  = null;
    ResultSet profile_rs    = null;

    // MySQL database objects
    Connection mysql_conn             = null;
    Statement mysql_stmt              = null;
    PreparedStatement mysql_pstmt_add = null;
    PreparedStatement mysql_pstmt_upd = null;
    ResultSet mysql_rs                = null;

    // Program's return value
    int returnval = 0;

    if (args.length < 3)
    {   
      System.out.println("Usage: java SyncUser <mysql jdbc-url> <user> <passwd> [profile uid]");
      System.exit(returnval);
    }   

    // get command line parameters
    String mysql_dbURL    = args[0];
    String mysql_dbUser   = args[1];
    String mysql_dbPasswd = args[2];

    // optional parameters
    String sync_uid = null;
    if (args.length > 3) sync_uid = args[3];

    try
    {
      // create MySQL connection
      System.out.println("Connecting to MySQL: " + mysql_dbURL);
      Class.forName(mysql_dbDriver);
      mysql_conn = DriverManager.getConnection(mysql_dbURL, mysql_dbUser, mysql_dbPasswd);
      mysql_stmt = mysql_conn.createStatement();

      // build PROFILE db-url
      String sync_host        = readPreference(mysql_stmt, "SYNC_HOST");
      String sync_port        = readPreference(mysql_stmt, "SYNC_PORT");
      String profile_dbUser   = readPreference(mysql_stmt, "SYNC_USER");
      String profile_dbPasswd = readPreference(mysql_stmt, "SYNC_PASSWD");
      profile_dbPasswd        = new String(PasswordTransform.decrypt(profile_dbPasswd));
      String profile_dbURL    = "protocol=jdbc:sanchez/database=" + sync_host + ":" + sync_port + ":SCA$IBS/rowPrefetch=200";

      // create PROFILE connection
      System.out.println("Connecting to PROFILE: " + profile_dbURL);
      Class.forName(profile_dbDriver);
      profile_conn = DriverManager.getConnection(profile_dbURL, profile_dbUser, profile_dbPasswd);
      profile_stmt = profile_conn.createStatement();
      profile_stmt.setMaxRows(0);

      // prepare MySQL add and update statement
      // add hubcode  add and upd change Position
      // add brgrp and upd Change Position
      mysql_pstmt_add = mysql_conn.prepareStatement("insert into user (uid,name,class,passwd,brcd,pwdfail,last,expire,hubcode,brgrp,sync) " +
                                                    "values (?,?,?,?,?,?,?,?,?,?,0)");
      mysql_pstmt_upd = mysql_conn.prepareStatement("update user set name = ?,class = ?,passwd = ?,brcd = ?," +
                                                    "last = ?,expire = ?,hubcode = ?,brgrp = ?,sync = 0 " +
                                                    "where uid = ?");
      // turn on un-sync bit field on MySQL
      if (sync_uid == null)
      {
        mysql_stmt.executeUpdate("update user set sync = 1");
        profile_rs = profile_stmt.executeQuery("select UID,%UFN,%UCLS,PSWDAUT,BRCD,PWDFAIL,PEXPR,ZHUB,ZBRGRP from SCAU");
      }
      else
      {
        mysql_stmt.executeUpdate("update user set sync = 1 where uid = '" + sync_uid + "'");
        profile_rs = profile_stmt.executeQuery("select UID,%UFN,%UCLS,PSWDAUT,BRCD,PWDFAIL,PEXPR,ZHUB,ZBRGRP from SCAU where UID = '" + sync_uid + "'");
      }

      System.out.println();
      while (profile_rs.next())
      {
        String recinfo = "";
        try
        {
          String uid     = profile_rs.getString("UID");
          String ufn     = new String(profile_rs.getBytes("%UFN"),"TIS620");
          String ucls    = profile_rs.getString("%UCLS");
          String pswdaut = profile_rs.getString("PSWDAUT");
          String brcd    = profile_rs.getString("BRCD");
          int pwdfail    = profile_rs.getInt("PWDFAIL");
          String pexpr   = profile_rs.getString("PEXPR");
          String hubcode = profile_rs.getString("ZHUB");
          String brgrp = profile_rs.getString("ZBRGRP");

          // eliminate null value from PROFILE
          if (brcd == null)  brcd  = new String("");
          if (hubcode == null)  hubcode  = new String("");
          if (brgrp == null)  brgrp  = new String("");
          if (pexpr == null) pexpr = new String("2020-01-01");

          recinfo = uid + ":" + ufn + ":" + ucls + ":" + pswdaut + ":" + brcd + ":" + pwdfail + ":" + pexpr + ":" + hubcode + ":" + brgrp;

          // fill-in MySQL update statement
          mysql_pstmt_upd.setString(9, uid);
          mysql_pstmt_upd.setString(1, ufn);
          mysql_pstmt_upd.setString(2, ucls);
          mysql_pstmt_upd.setString(3, pswdaut);
          mysql_pstmt_upd.setString(4, brcd);
          mysql_pstmt_upd.setTime(5, new Time(0));
          mysql_pstmt_upd.setString(6, pexpr);
          mysql_pstmt_upd.setString(7, hubcode);
          mysql_pstmt_upd.setString(8, brgrp);

          // try to perform MySQL update
          if (mysql_pstmt_upd.executeUpdate() == 1)
            System.out.println("[Updated] " + recinfo);

          else     // record not found, add new record
          {
            mysql_pstmt_add.setString(1, uid);
            mysql_pstmt_add.setString(2, ufn);
            mysql_pstmt_add.setString(3, ucls);
            mysql_pstmt_add.setString(4, pswdaut);
            mysql_pstmt_add.setString(5, brcd);
            mysql_pstmt_add.setInt(6, pwdfail);
            mysql_pstmt_add.setTime(7, new Time(0));
            mysql_pstmt_add.setString(8, pexpr);
            mysql_pstmt_add.setString(9, hubcode);
            mysql_pstmt_add.setString(10, brgrp);

            if (mysql_pstmt_add.executeUpdate() != 1)
            {
              System.out.println("[Error]   " + recinfo);
              returnval = 1;
            }

            System.out.println("[Added]   " + recinfo);
          }
        }
        catch (SQLException e)
        {
          System.out.println("[Error]   " + recinfo);
          e.printStackTrace();
          returnval = 1;
        }
      }

      // delete un-touched record
      if (mysql_stmt.executeUpdate("delete from user where sync = 1") > 0)
        System.out.println("Inexistence user deleted");
    }
    finally
    {
      if (profile_rs   != null) try { profile_rs.close();   profile_rs   = null; } catch (Exception e) {}
      if (profile_stmt != null) try { profile_stmt.close(); profile_stmt = null; } catch (Exception e) {}
      if (profile_conn != null) try { profile_conn.close(); profile_conn = null; } catch (Exception e) {}

      if (mysql_pstmt_upd != null) try { mysql_pstmt_upd.close(); mysql_pstmt_upd = null; } catch (Exception e) {}
      if (mysql_pstmt_add != null) try { mysql_pstmt_add.close(); mysql_pstmt_add = null; } catch (Exception e) {}
      if (mysql_stmt      != null) try { mysql_stmt.close();      mysql_stmt      = null; } catch (Exception e) {}
      if (mysql_conn      != null) try { mysql_conn.close();      mysql_conn      = null; } catch (Exception e) {}
    }

    System.exit(returnval);
  }
}
