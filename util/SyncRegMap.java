import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// Revision History
//
// 02-03-2012 Modified by Tueng+
//            - add try/catch block while fetching data from Profile and updating to MySQL
//              in order to protect program termination by invalid data value

public class SyncRegMap
{
  private static String profile_dbDriver = "sanchez.jdbc.driver.ScDriver";
  private static String mysql_dbDriver   = "com.mysql.jdbc.Driver";

  private static String readPreference(Statement stmt, String varname)
    throws SQLException
  {
    ResultSet rs    = null;
    String varvalue = null;

    try
    {
      rs = stmt.executeQuery("select varvalue from preferences where varname = '" + varname + "'");
      if (rs.next())
        varvalue = rs.getString("varvalue");
      else
        throw new SQLException("No preference variable: " + varname);
    }
    finally
    {
      if (rs != null) try { rs.close(); rs = null; } catch (Exception e) {}
    }

    return varvalue;
  }

  public static void main(String args[])
    throws ClassNotFoundException, SQLException, UnsupportedEncodingException
  {
    // PROFILE database objects
    Connection profile_conn = null;
    Statement profile_stmt  = null;
    ResultSet profile_rs    = null;
    ResultSet profile_rs2    = null;

    // MySQL database objects
    Connection mysql_conn         = null;
    PreparedStatement mysql_pstmt_add = null;
    PreparedStatement mysql_pstmt_update = null;
    Statement mysql_stmt          = null;

    // Program's return value
    int returnval = 0;

    if (args.length < 3)
    {
      System.out.println("Usage: java SyncRegMap <mysql jdbc-url> <user> <passwd>");
      System.exit(returnval);
    }

    // get command line parameters
    String mysql_dbURL    = args[0];
    String mysql_dbUser   = args[1];
    String mysql_dbPasswd = args[2];

    try
    {
      // create MySQL connection
      System.out.println("Connecting to MySQL: " + mysql_dbURL);
      Class.forName(mysql_dbDriver);
      mysql_conn = DriverManager.getConnection(mysql_dbURL, mysql_dbUser, mysql_dbPasswd);
      mysql_stmt = mysql_conn.createStatement();

      // build PROFILE db-url
      String sync_host        = readPreference(mysql_stmt, "SYNC_HOST");
      String sync_port        = readPreference(mysql_stmt, "SYNC_PORT");
      String profile_dbUser   = readPreference(mysql_stmt, "SYNC_USER");
      String profile_dbPasswd = readPreference(mysql_stmt, "SYNC_PASSWD");
      profile_dbPasswd        = new String(PasswordTransform.decrypt(profile_dbPasswd));
      String profile_dbURL    = "protocol=jdbc:sanchez/database=" + sync_host + ":" + sync_port + ":SCA$IBS/rowPrefetch=200";

      // create PROFILE connection
      System.out.println("Connecting to PROFILE: " + profile_dbURL);
      Class.forName(profile_dbDriver);
      profile_conn = DriverManager.getConnection(profile_dbURL, profile_dbUser, profile_dbPasswd);
      profile_stmt = profile_conn.createStatement();
      profile_stmt.setMaxRows(0);

      // delete old regmap
      System.out.println();
      System.out.println("Delete existing data");
      mysql_stmt.executeUpdate("truncate regmap");

      mysql_pstmt_add = mysql_conn.prepareStatement("insert into regmap (reg,brcd) values (?,?)");
      mysql_pstmt_update = mysql_conn.prepareStatement("update regmap set brcd = ? where reg = ? and brcd = ?");

      profile_rs = profile_stmt.executeQuery("select REG,BRCD from ZUTBLREGREP");
      while (profile_rs.next())
      {
        String recinfo = "";
        try
        {
          String reg  = profile_rs.getString("REG");
          String brcd = profile_rs.getString("BRCD");

          recinfo = reg + ":" + brcd;

          // fill-in MySQL update statement
          mysql_pstmt_add.setString(1,reg);
          mysql_pstmt_add.setString(2,brcd);

          if (mysql_pstmt_add.executeUpdate() == 1)
            System.out.println("[Added] " + recinfo);
          else
          {
            System.out.println("[Error] " + recinfo);
            returnval = 1;
          }
        }
        catch (SQLException e)
        {
          System.out.println("[Error] " + recinfo);
          e.printStackTrace();
          returnval = 1;
        }
      }

      profile_rs2 = profile_stmt.executeQuery("select DIST,BRCD from ZUTBLREGREP");
      while (profile_rs2.next())
      {
        String dist  = profile_rs2.getString("DIST");
        String brcd = profile_rs2.getString("BRCD");

        String recinfo = dist + ":" + brcd;

        // fill-in MySQL update statement
        mysql_pstmt_update.setString(1,brcd);
        mysql_pstmt_update.setString(2,dist);
        mysql_pstmt_update.setString(3,brcd);
	if (mysql_pstmt_update.executeUpdate() == 1)
		System.out.println("[Update] " + recinfo);
        else
	{
	        mysql_pstmt_add.setString(1,dist);
	        mysql_pstmt_add.setString(2,brcd);

		if (mysql_pstmt_add.executeUpdate() == 1)
	          System.out.println("[Added] " + recinfo);
	        else
	        {
	          System.out.println("[Error] " + recinfo);
	          returnval = 1;
	        }
	}
      }
    }
    finally
    {
      if (profile_rs   != null) try { profile_rs.close();   profile_rs   = null; } catch (Exception e) {}
      if (profile_rs2   != null) try { profile_rs2.close();   profile_rs2   = null; } catch (Exception e) {}
      if (profile_stmt != null) try { profile_stmt.close(); profile_stmt = null; } catch (Exception e) {}
      if (profile_conn != null) try { profile_conn.close(); profile_conn = null; } catch (Exception e) {}

      if (mysql_pstmt_add != null) try { mysql_pstmt_add.close(); mysql_pstmt_add = null; } catch (Exception e) {}
      if (mysql_pstmt_update != null) try { mysql_pstmt_update.close(); mysql_pstmt_update = null; } catch (Exception e) {}
      if (mysql_stmt  != null) try { mysql_stmt.close();  mysql_stmt  = null; } catch (Exception e) {}
      if (mysql_conn  != null) try { mysql_conn.close();  mysql_conn  = null; } catch (Exception e) {}
    }

    System.exit(returnval);
  }
}
