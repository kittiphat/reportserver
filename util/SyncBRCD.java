import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// Revision History
//
// 30-11-2005 add hubcode by rawee  
//
// 23-03-2006 add brgrp by rawee
//
// 02-03-2012 Modified by Tueng+
//            - add try/catch block while fetching data from Profile and updating to MySQL
//              in order to protect program termination by invalid data value
//            - truncate branch description to 40 characters to match with field length

public class SyncBRCD
{
  private static String profile_dbDriver = "sanchez.jdbc.driver.ScDriver";
  private static String mysql_dbDriver   = "com.mysql.jdbc.Driver";

  private static String readPreference(Statement stmt, String varname)
    throws SQLException
  {
    ResultSet rs    = null;
    String varvalue = null;

    try
    {
      rs = stmt.executeQuery("select varvalue from preferences where varname = '" + varname + "'");
      if (rs.next())
        varvalue = rs.getString("varvalue");
      else
        throw new SQLException("No preference variable: " + varname);
    }
    finally
    {
      if (rs != null) try { rs.close(); rs = null; } catch (Exception e) {}
    }

    return varvalue;
  }

  public static void main(String args[])
    throws ClassNotFoundException, SQLException, UnsupportedEncodingException
  {
    // PROFILE database objects
    Connection profile_conn = null;
    Statement profile_stmt  = null;
    ResultSet profile_rs    = null;

    // MySQL database objects
    Connection mysql_conn         = null;
    PreparedStatement mysql_pstmt = null;
    Statement mysql_stmt          = null;

    // Program's return value
    int returnval = 0;

    if (args.length < 3)
    {   
      System.out.println("Usage: java SyncBRCD <mysql jdbc-url> <user> <passwd>");
      System.exit(returnval);
    }

    // get command line parameters
    String mysql_dbURL    = args[0];
    String mysql_dbUser   = args[1];
    String mysql_dbPasswd = args[2];

    try
    {
      // create MySQL connection
      System.out.println("Connecting to MySQL: " + mysql_dbURL);
      Class.forName(mysql_dbDriver);
      mysql_conn = DriverManager.getConnection(mysql_dbURL, mysql_dbUser, mysql_dbPasswd);
      mysql_stmt = mysql_conn.createStatement();

      // build PROFILE db-url 
      String sync_host        = readPreference(mysql_stmt, "SYNC_HOST");
      String sync_port        = readPreference(mysql_stmt, "SYNC_PORT");
      String profile_dbUser   = readPreference(mysql_stmt, "SYNC_USER");
      String profile_dbPasswd = readPreference(mysql_stmt, "SYNC_PASSWD");
      profile_dbPasswd        = new String(PasswordTransform.decrypt(profile_dbPasswd));
      String profile_dbURL    = "protocol=jdbc:sanchez/database=" + sync_host + ":" + sync_port + ":SCA$IBS/rowPrefetch=200";

      // create PROFILE connection
      System.out.println("Connecting to PROFILE: " + profile_dbURL);
      Class.forName(profile_dbDriver);
      profile_conn = DriverManager.getConnection(profile_dbURL, profile_dbUser, profile_dbPasswd);
      profile_stmt = profile_conn.createStatement();
      profile_stmt.setMaxRows(0);

      // delete old brcd
      System.out.println();
      System.out.println("Delete existing data");
      mysql_stmt.executeUpdate("truncate brcd");

      mysql_pstmt = mysql_conn.prepareStatement("insert into brcd (brcd,descr,ccdef,bocflg,bocreg,hubcode,brgrp) values (?,?,?,?,?,?,?)");

      profile_rs = profile_stmt.executeQuery("select UTBLBRCD.BRCD,UTBLBRCD.DESC,UTBLBRCD.CCDEF,ZUTBLBRCD.BOCFLG,ZUTBLBRCD.BOCREG,ZUTBLBRCD.ZHUB,ZUTBLBRCD.ZBRGRP " +
                                             "from UTBLBRCD left join ZUTBLBRCD on (UTBLBRCD.BRCD = ZUTBLBRCD.BRCD)");
      while (profile_rs.next())
      {
        String recinfo = "";
        try
        {
          int brcd      = profile_rs.getInt("BRCD");
          String desc   = new String(profile_rs.getBytes("DESC"),"TIS620");
          int ccdef     = profile_rs.getInt("CCDEF");
          String bocflg = profile_rs.getString("BOCFLG");
          String bocreg = profile_rs.getString("BOCREG");
          String hubcode = profile_rs.getString("ZHUB");
          String brgrp = profile_rs.getString("ZBRGRP");

          // eliminate null value from PROFILE
          if (bocreg == null) bocreg = new String("");
          if (hubcode == null) hubcode = new String("");
          if (brgrp == null) brgrp = new String("");

          // translate logical field to "Y" or "N"
          if (bocflg.equals("true"))
            bocflg = "Y";
          else
            bocflg = "N";

          // truncate branch description down to 40 characters
          if (desc.length() > 40)
          {
            String newdesc = desc.substring(0,40);
            System.out.println("[Warning] branch desc truncated from [" + desc + "] -> [" + newdesc + "]");
            desc = newdesc;
            returnval = 1;
          }

          recinfo = brcd + ":" + desc + ":" + ccdef + ":" + bocflg + ":" + bocreg + ":" + hubcode+ ":" + brgrp;

          // fill-in MySQL update statement
          mysql_pstmt.setInt(1,brcd);
          mysql_pstmt.setString(2,desc);
          mysql_pstmt.setInt(3,ccdef);
          mysql_pstmt.setString(4,bocflg);
          mysql_pstmt.setString(5,bocreg);
          mysql_pstmt.setString(6,hubcode);
          mysql_pstmt.setString(7,brgrp);

          if (mysql_pstmt.executeUpdate() == 1)
            System.out.println("[Added] " + recinfo);
          else
          {
            System.out.println("[Error] " + recinfo);
            returnval = 1;
          }
        }
        catch (SQLException e)
        {
          System.out.println("[Error] " + recinfo);
          e.printStackTrace();
          returnval = 1;
        }
      }
    }
    finally
    {
      if (profile_rs   != null) try { profile_rs.close();   profile_rs   = null; } catch (Exception e) {}
      if (profile_stmt != null) try { profile_stmt.close(); profile_stmt = null; } catch (Exception e) {}
      if (profile_conn != null) try { profile_conn.close(); profile_conn = null; } catch (Exception e) {}

      if (mysql_pstmt != null) try { mysql_pstmt.close(); mysql_pstmt = null; } catch (Exception e) {}
      if (mysql_stmt  != null) try { mysql_stmt.close();  mysql_stmt  = null; } catch (Exception e) {}
      if (mysql_conn  != null) try { mysql_conn.close();  mysql_conn  = null; } catch (Exception e) {}
    }

    System.exit(returnval);
  }
}
