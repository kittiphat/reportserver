package com.tnis.reportapp;

import java.sql.Date;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public final class DownloadAction extends Action
{
  private Log log     = LogFactory.getLog("com.tnis.reportapp");
  private int bufSize = 8192;

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    log.debug("invoke DownloadAction");
    ActionErrors  actionErrors  = new ActionErrors();
    ActionForward actionForward = null;

    try
    {
      // servlet input parameters
      String code  = (String)PropertyUtils.getSimpleProperty(form, "code");
      String freq  = (String)PropertyUtils.getSimpleProperty(form, "freq");
      String brcd  = (String)PropertyUtils.getSimpleProperty(form, "brcd");
      Date repdate = (Date)PropertyUtils.getSimpleProperty(form, "repdate");

      if (log.isDebugEnabled())
      {
        log.debug("input code    = " + code);
        log.debug("input freq    = " + freq);
        log.debug("input brcd    = " + brcd);
        log.debug("input repdate = " + repdate);
      }

      if (code == null || code.equals("") ||
          brcd == null || brcd.equals("") ||
          repdate == null)
      {
        throw new DownloadException("Incomplete input parameter");
      }

      // get loginBean from session
      LoginBean loginBean = Util.getLoginBean(request);

      DownloadBean downloadBean = new DownloadBean(loginBean);
      downloadBean.findRepFile(code, freq, brcd, repdate);

      log.info("user [" + loginBean.getUid() + "] from [" + request.getRemoteAddr() + 
               "] is downloading [" + downloadBean.getRepFile().getFile() + "]");
      sendFile(response, downloadBean.getRepFile().getFile());
    }
    catch (NoSessionException e)
    {
      log.error(e);
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("sessionTimeOut"));
      actionForward = mapping.findForward("Index");
    }
    catch (DownloadException e)
    {
      log.error(e);
      e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("reportListError", e.getMessage()));
      actionForward = mapping.findForward("ReportList");
    }

    if (!actionErrors.isEmpty())
    {
      saveErrors(request, actionErrors);
    }

    return actionForward;
  }

  private void sendFile(HttpServletResponse response, String fileName)
    throws DownloadException, IOException
  {
    FileInputStream infile      = null;
    BufferedInputStream bufin   = null;
    BufferedOutputStream bufout = null;

    try
    {
      File file = new File(fileName);
      if ((!file.exists()) || (!file.canRead()))
      {
        throw new DownloadException("Cannot read file " + fileName);
      }

      infile = new FileInputStream(file);
      bufin  = new BufferedInputStream(infile, bufSize);
      bufout = new BufferedOutputStream(response.getOutputStream());

      response.setContentLength((int)file.length());
      response.setContentType("application/pdf");
      response.setHeader("Content-disposition", "attachment; filename=" + file.getName());

      int res = 0;
      while (res != -1)
      {
        res = bufin.read();
        bufout.write(res);
      }
    }
    finally
    {
      if (bufin  != null) bufin.close();
      if (bufout != null) bufout.flush();
      if (infile != null) infile.close();
    }
  }
}
