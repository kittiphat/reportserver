package com.tnis.reportapp;

import java.sql.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportFileBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  public static final int NO_REPORT = 0;
  public static final int NO_DATA   = 1;
  public static final int ONLINE    = 2;

  private String code     = null;
  private String name     = null;
  private String descr    = null;
  private Date repdate    = null;
  private String file     = null;
  private int size        = 0;
  private String note     = null;
  private int status      = NO_REPORT;

  public ReportFileBean()
  {
    log.debug("create ReportFileBean");
  }
  
  // getter method
  public String  getCode()    { return this.code;    }
  public String  getName()    { return this.name;    }
  public String  getDescr()   { return this.descr;   }
  public Date    getRepDate() { return this.repdate; }
  public String  getFile()    { return this.file;    }
  public int     getSize()    { return this.size;    }
  public String  getNote()    { return this.note;    }
  public int     getStatus()  { return this.status;  }

  // setter method
  public void setCode(String code)     { this.code    = code;    }
  public void setName(String name)     { this.name    = name;    }
  public void setDescr(String descr)   { this.descr   = descr;   }
  public void setRepDate(Date repdate) { this.repdate = repdate; }
  public void setFile(String file)     { this.file    = file;    }
  public void setSize(int size)        { this.size    = size;    }
  public void setNote(String note)     { this.note    = note;    }
  public void setStatus(int status)    { this.status  = status;  }
}
