package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

public class ReportListBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  // initialize in constructor
  private Vector brcdV      = null;
  private Vector repclassV  = null;

  // change by findRepList methods
  private boolean isnew     = true;
  private String  freq      = null;
  private String  brcd      = null;
  private Integer repclass  = null;
  private String  brname    = null;
  private Date    repdate   = null;
  private Vector  repdateV  = null;
  private Date    postdate  = null;
  private Vector  postdateV = null;
  private Vector  repfileV  = null;

  public ReportListBean(LoginBean loginBean)
    throws ReportListException, LoginException
  {
    if (log.isDebugEnabled())
    {
      log.debug("create ReportListBean");
      log.debug("brcdV     = " + loginBean.getBrcdV());
      log.debug("repclassV = " + loginBean.getRepClassV());
    }
    this.brcdV     = loginBean.getBrcdV();
    this.repclassV = loginBean.getRepClassV();
  }

  // getter methods
  public boolean isNew()
  {
    return this.isnew;
  }

  public String getFreq() throws ReportListException
  {
    if (this.freq == null)
    {
      throw new ReportListException("freq not initialized");
    }
    return this.freq;
  }

  public String getBrcd() throws ReportListException
  {
    if (this.brcd == null)
    {
      throw new ReportListException("brcd not initialized");
    }
    return this.brcd;
  }

  public Integer getRepClass() throws ReportListException
  {
    if (this.repclass == null)
    {
      throw new ReportListException("repclass not initialized");
    }
    return this.repclass;
  }

  public String getBrName() throws ReportListException
  {
    if (this.brname == null)
    {
      throw new ReportListException("brname not initialized");
    }
    return this.brname;
  }

  public Date getRepDate() throws ReportListException
  {
    if (this.repdate == null)
    {
      throw new ReportListException("repdate not initialized");
    }
    return this.repdate;
  }

  public Vector getRepDateV() throws ReportListException
  {
    if (this.repdateV == null)
    {
      throw new ReportListException("repdateV not initialized");
    }
    return this.repdateV;
  }

  public Date getPostDate() throws ReportListException
  {
    if (this.postdate == null)
    {
      throw new ReportListException("postdate not initialized");
    }
    return this.postdate;
  }

  public Vector getPostDateV() throws ReportListException
  {
    if (this.postdateV == null)
    {
      throw new ReportListException("postdateV not initialized");
    }
    return this.postdateV;
  }

  public Vector getRepFileV() throws ReportListException
  {
    if (this.repfileV == null)
    {
      throw new ReportListException("repfileV not initialized");
    }
    return repfileV;
  }

  public void findRepList(String freq, String brcd, Integer repclass, Date repdate)
    throws ReportListException, IOException, NamingException, SQLException
  {
    Connection conn    = null;
    Statement stmt     = null;
    ResultSet rs       = null;
    Vector repdateV1   = new Vector();
    Vector repfileV1   = new Vector(100);
    Hashtable repfileH = new Hashtable(100);

    if (log.isDebugEnabled())
    {
      log.debug("invoke findRepList(freq, brcd, repclass, repdate)");
      log.debug("freq     = " + freq);
      log.debug("brcd     = " + brcd);
      log.debug("repclass = " + repclass);
      log.debug("repdate  = " + repdate);
    }

    // using cached data if all input parameters is the same
    if ((this.freq     != null) && freq.equals(this.freq) &&
        (this.brcd     != null) && brcd.equals(this.brcd) &&
        (this.repclass != null) && repclass.equals(this.repclass) &&
        (this.repdate  != null) && (repdate != null) && repdate.equals(this.repdate))
    {
      log.debug("using cached data");
      return;
    }

    // initialize private variables
    this.isnew    = false;
    this.freq     = freq;
    this.brcd     = brcd;
    this.repclass = repclass;
    this.repdate  = new Date(0);
    this.repdateV = new Vector();
    this.repfileV = new Vector();

    // check branch permission
    if (!Util.checkBrcdPermission(this.brcdV, brcd))
    {
      throw new ReportListException("No permission on branch " + brcd);
    }

    // check report class permission
    if (!Util.checkRepClassPermission(this.repclassV, repclass))
    {
      throw new ReportListException("No permission on report class " + repclass);
    }

    // if not specify repdate, find report date by frequency and select latest date
    if (repdate == null)
    {
      this.repdateV = findRepDateByFreq(freq);
      this.repdate  = (Date)repdateV.firstElement();
    }
    else
    {
      repdateV1    = findRepDateByFreq(freq);
      this.repdate = repdate;
      if (!repdateV1.contains(repdate))
      {
        throw new ReportListException("No report on " + repdate.toString());
      }
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      // 1st round, create master report list from repcode
      log.debug("create master report list from repcode");
      rs   = stmt.executeQuery("select code,name,descr from repcode " + 
                               "where class = " + this.repclass + " and " +
                               "freq = '" + this.freq + "' order by code");
      while (rs.next())
      {
        String repcode = rs.getString("code");
        ReportFileBean repfile = new ReportFileBean();
        repfile.setCode(repcode);
        repfile.setName(rs.getString("name"));
        repfile.setDescr(rs.getString("descr"));
        repfile.setStatus(ReportFileBean.NO_REPORT);
        repfileV1.addElement(repfile);
        repfileH.put(repcode, new Integer(repfileV1.size() - 1));
      }

      // 2nd round, find posted report code from repfile_by_repdate
      log.debug("find posted report code from repfile_by_repdate");
      rs = stmt.executeQuery("select code from repfile_by_repdate " +
                             "where freq = '" + this.freq + "' and " +
                             "repdate = '" + this.repdate+ "'");
      while (rs.next())
      {
        Integer i = (Integer)repfileH.get(rs.getString("code"));
        if (i == null) continue;

        ReportFileBean repfile = (ReportFileBean)repfileV1.elementAt(i.intValue());
        repfile.setStatus(ReportFileBean.NO_DATA);

        repfileV1.setElementAt(repfile, i.intValue());
      }

      // 3rd round, find available report from repfile
      log.debug("find available report from repfile");
      rs = stmt.executeQuery("select rc.code,rf.repdate,rf.file,rf.size,rf.note " +
                             "from repcode as rc join repfile as rf using (code,freq) " +
                             "where rc.class = '" + this.repclass + "' and " +
                             "rf.freq = '" + this.freq + "' and " +
                             "rf.brcd = '" + this.brcd + "' and " +
                             "rf.repdate = '" + this.repdate + "'");
      while (rs.next())
      {
        Integer i = (Integer)repfileH.get(rs.getString("code"));
        if (i == null) continue;

        ReportFileBean repfile = (ReportFileBean)repfileV1.elementAt(i.intValue());
        repfile.setRepDate(rs.getDate("repdate"));
        repfile.setFile(rs.getString("file"));
        repfile.setSize(rs.getInt("size"));
        repfile.setNote(rs.getString("note"));
        repfile.setStatus(ReportFileBean.ONLINE);
        repfileV1.setElementAt(repfile, i.intValue());
      }

      this.repfileV = repfileV1;

      if (repdate != null)
      {
        this.repdateV = repdateV1;
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }

  public void findRepListOnReq(String freq, String brcd, Integer repclass, Date postdate)
    throws ReportListException, IOException, NamingException, SQLException
  {
    Connection conn    = null;
    Statement stmt     = null;
    ResultSet rs       = null;
    Vector postdateV1  = new Vector();
    Vector repfileV1   = new Vector(100);
    Hashtable repfileH = new Hashtable(100);

    log.debug("invoke findRepListOnReq(freq, brcd, repclass, postdate)");
    log.debug("freq     = " + freq);
    log.debug("brcd     = " + brcd);
    log.debug("repclass = " + repclass);
    log.debug("postdate = " + postdate);

    // using cached data if all input parameters is the same
    if ((this.freq     != null) && freq.equals(this.freq) &&
        (this.brcd     != null) && brcd.equals(this.brcd) &&
        (this.repclass != null) && repclass.equals(this.repclass) &&
        (this.postdate != null) && (postdate != null) && postdate.equals(this.postdate))
    {
      log.debug("using cached data");
      return;
    }

    // initialize private variables
    this.freq      = freq;
    this.brcd      = brcd;
    this.repclass  = repclass;
    this.postdate  = new Date(0);
    this.postdateV = new Vector();
    this.repfileV  = new Vector();

    // check branch permission
    if (!this.brcdV.contains(brcd))
    {
      throw new ReportListException("No permission on " + brcd + " report");
    }

    // check report class permission
    // ***** TO DO *****


    // if not specify postdate, find report date by frequency and select latest date
    if (postdate == null)
    {
      this.postdateV = findPostDateByFreq(freq);
      this.postdate  = (Date)postdateV.firstElement();
    }
    else
    {
      postdateV1     = findPostDateByFreq(freq);
      this.postdate  = postdate;
      if (!postdateV1.contains(postdate))
      {
        throw new ReportListException("No report on " + postdate.toString());
      }
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery("select rc.code,rc.name,rc.descr,rf.repdate,rf.file,rf.size,rf.note " +
                             "from repcode as rc join repfile as rf using (code,freq) " +
                             "where rc.class = " + this.repclass + " and " +
                             "rc.freq = '" + this.freq + "' and " +
                             "rf.brcd = '" + this.brcd + "' and " +
                             "rf.postdate = '" + this.postdate + "' order by rc.code,rf.repdate");
      while (rs.next())
      {
        ReportFileBean repfile = new ReportFileBean();

        repfile.setCode(rs.getString("code"));
        repfile.setName(rs.getString("name"));
        repfile.setDescr(rs.getString("descr"));
        repfile.setRepDate(rs.getDate("repdate"));
        repfile.setFile(rs.getString("file"));
        repfile.setSize(rs.getInt("size"));
        repfile.setNote(rs.getString("note"));
        repfile.setStatus(ReportFileBean.ONLINE);

        repfileV1.addElement(repfile);
      }

      this.repfileV = repfileV1;

      if (postdate != null)
      {
        this.postdateV = postdateV1;
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }

  private Vector findRepDateByFreq(String freq)
    throws ReportListException, IOException, NamingException, SQLException
  {
    Connection conn  = null;
    Statement stmt   = null;
    ResultSet rs     = null;
    Vector repdateV1 = new Vector();

    log.debug("invoke findRepDateByFreq");
    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();
      log.debug("select date from repdate where freq = '" + freq + "' order by date desc");
      rs = stmt.executeQuery("select date from repdate where freq = '" + freq + "' order by date desc");
      while (rs.next())
      {
        repdateV1.addElement(rs.getDate("date"));
      }
      if (repdateV1.isEmpty())
      {
        throw new ReportListException("No report on this frequency");
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }

    return repdateV1;
  }

  private Vector findPostDateByFreq(String freq)
    throws ReportListException, IOException, NamingException, SQLException
  {
    Connection conn   = null;
    Statement stmt    = null;
    ResultSet rs      = null;
    Vector postdateV1 = new Vector();

    log.debug("invoke findPostDateByFreq");
    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();
      log.debug("select date from postdate where freq = '" + freq + "' order by date desc");
      rs = stmt.executeQuery("select date from postdate where freq = '" + freq + "' order by date desc");
      while (rs.next())
      {
        postdateV1.addElement(rs.getDate("date"));
      }
      if (postdateV1.isEmpty())
      {
        throw new ReportListException("No report on given frequency");
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }

    return postdateV1;
  }
}
