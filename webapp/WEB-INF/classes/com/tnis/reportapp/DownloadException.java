package com.tnis.reportapp;

public class DownloadException extends Exception
{
  public DownloadException()
  {
    super();
  }

  public DownloadException(String msg)
  {
    super(msg);
  }
}
