package com.tnis.reportapp;

public class LoginException extends Exception
{
  public LoginException()
  {
    super();
  }

  public LoginException(String msg)
  {
    super(msg);
  }
}
