package com.tnis.reportapp;

import java.sql.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReportArchiveBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  public static final int NO_DATA   = 0;
  public static final int ON_TAPE   = 1;
  public static final int REQUESTED = 2;
  public static final int ONLINE    = 3;

  private String  code         = null;
  private String  freq         = null;
  private String  name         = null;
  private String  repclass     = null;
  private Date    postdate     = null;
  private int     status       = NO_DATA;
  private String  requid       = null;
  private boolean permitcancel = false;

  public ReportArchiveBean()
  {
    log.debug("create ReportArchiveBean");
  }
  
  // getter method
  public String  getCode()         { return this.code;         }
  public String  getFreq()         { return this.freq;         }
  public String  getName()         { return this.name;         }
  public String  getRepClass()     { return this.repclass;     }
  public Date    getPostDate()     { return this.postdate;     }
  public int     getStatus()       { return this.status;       }
  public String  getReqUid()       { return this.requid;       }
  public boolean getPermitCancel() { return this.permitcancel; }

  // setter method
  public void setCode(String code)                  { this.code         = code;         }
  public void setFreq(String freq)                  { this.freq         = freq;         }
  public void setName(String name)                  { this.name         = name;         }
  public void setRepClass(String repclass)          { this.repclass     = repclass;     }
  public void setPostDate(Date postdate)            { this.postdate     = postdate;     }
  public void setStatus(int status)                 { this.status       = status;       }
  public void setReqUid(String requid)              { this.requid       = requid;       }
  public void setPermitCancel(boolean permitcancel) { this.permitcancel = permitcancel; }
}
