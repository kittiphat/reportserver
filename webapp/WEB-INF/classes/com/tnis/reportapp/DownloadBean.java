package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DownloadBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  // initialize in constructor
  private Vector brcdV      = null;
  private Vector repclassV  = null;

  private ReportFileBean repfile = null;

  public DownloadBean(LoginBean loginBean) throws LoginException
  {
    if (log.isDebugEnabled())
    {
      log.debug("create DownloadBean");
      log.debug("brcdV     = " + loginBean.getBrcdV());
      log.debug("repclassV = " + loginBean.getRepClassV());
    }
    this.brcdV     = loginBean.getBrcdV();
    this.repclassV = loginBean.getRepClassV();
  }
  
  // getter method
  public ReportFileBean getRepFile() throws DownloadException
  {
    if (repfile == null)
    {
      throw new DownloadException("DownloadBean not initialized");
    }
    return this.repfile;
  }

  public void findRepFile(String code, String freq, String brcd, Date repdate)
    throws DownloadException, IOException, NamingException, SQLException
  {
    Connection conn         = null;
    Statement stmt          = null;
    ResultSet rs            = null;
    Integer repclass        = null;
    ReportFileBean repfile1 = new ReportFileBean();

    if (log.isDebugEnabled())
    {
      log.debug("invoke findRepFile(code, brcd, repdate)");
      log.debug("code    = " + code);
      log.debug("freq    = " + freq);
      log.debug("brcd    = " + brcd);
      log.debug("repdate = " + repdate);
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      // check branch permission
      if (!Util.checkBrcdPermission(this.brcdV, brcd))
      {
        throw new DownloadException("No permission on branch " + brcd);
      }

      // check report class permission
      rs = stmt.executeQuery("select class from repcode where code = '" + code + "'");
      if (!rs.next())
      {
        throw new DownloadException("Invalid report code: " + code);
      }

      repclass = new Integer(rs.getInt("class"));
      log.debug("repclassV = " + repclassV);
      log.debug("repclass  = " + repclass);
      if (repclass == null)
      {
        throw new DownloadException("Report " + code + " has undefined report class");
      }

      if (!Util.checkRepClassPermission(this.repclassV, repclass))
      {
        throw new DownloadException("No permission on report " + code);
      }

      log.debug("select code,repdate,file,size from repfile " +
                "where code = '" + code +
                "' and freq = '" + freq +
                "' and brcd = '" + brcd +
                "' and repdate = '" + repdate + "'");
      rs = stmt.executeQuery("select code,repdate,file,size from repfile " +
                             "where code = '" + code +
                             "' and freq = '" + freq +
                             "' and brcd = '" + brcd +
                             "' and repdate = '" + repdate + "'");
      if (!rs.next())
      {
        throw new DownloadException("Report file not found");
      }

      repfile1.setCode(rs.getString("code"));
      repfile1.setRepDate(rs.getDate("repdate"));
      repfile1.setFile(rs.getString("file"));
      repfile1.setSize(rs.getInt("size"));

      this.repfile = repfile1;
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }
}
