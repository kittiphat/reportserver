package com.tnis.reportapp;

import java.sql.Date;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

public final class ReportListAction extends Action
{
  private Log log = LogFactory.getLog("com.tnis.reportapp");

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    log.debug("invoke ReportListAction");
    ActionErrors  actionErrors    = new ActionErrors();
    ActionForward actionForward   = null;
    ReportListBean reportListBean = null;

    try
    {
      // servlet input parameters
      Integer cmd      = (Integer)PropertyUtils.getSimpleProperty(form, "cmd");
      String freq      = (String)PropertyUtils.getSimpleProperty(form, "freq");
      String brcd      = (String)PropertyUtils.getSimpleProperty(form, "brcd");
      Integer repclass = (Integer)PropertyUtils.getSimpleProperty(form, "repclass");
      Date repdate     = (Date)PropertyUtils.getSimpleProperty(form, "repdate");
      Date postdate    = (Date)PropertyUtils.getSimpleProperty(form, "postdate");

      if (log.isDebugEnabled())
      {
        log.debug("input cmd      = " + cmd);
        log.debug("input freq     = " + freq);
        log.debug("input brcd     = " + brcd);
        log.debug("input repclass = " + repclass);
        log.debug("input repdate  = " + repdate);
        log.debug("input postdate = " + postdate);
      }

      // get loginBean from session
      LoginBean loginBean = Util.getLoginBean(request);

      // create reportListBean
      reportListBean = getReportListBean(request, loginBean);

      // initial cmd parameter
      if (cmd == null) cmd = new Integer(0);

      // initial frequency
      if (freq == null || freq.equals(""))
      {
        if ((cmd.intValue() == 1) || reportListBean.isNew())
          freq = "D";
        else
          freq = reportListBean.getFreq();
      }

      // initial branch code
      if (brcd == null || brcd.equals(""))
      {
        if ((cmd.intValue() == 1) || reportListBean.isNew())
        {
          brcd = loginBean.getBrcd();
          if (brcd == null || brcd.equals(""))
            brcd = (String)loginBean.getBrcdV().firstElement();
        }
        else
          brcd = reportListBean.getBrcd();
      }

      // initial report class
      if (repclass == null)
      {
        if ((cmd.intValue() == 1) || reportListBean.isNew())
        {
          LabelValueBean lv = (LabelValueBean)loginBean.getRepClassV().firstElement();
          repclass = new Integer(lv.getValue());
        }
        else
          repclass = reportListBean.getRepClass();
      }

      // initial report date or post date
      if (freq.equals("R"))
      {
        if ((postdate == null) && (cmd.intValue() != 1) && !reportListBean.isNew())
          postdate = reportListBean.getPostDate();
      }
      else
      {
        if ((repdate == null) && (cmd.intValue() != 1) && !reportListBean.isNew())
          repdate = reportListBean.getRepDate();
      }

      // set form bean for jsp display correction
      PropertyUtils.setSimpleProperty(form, "freq", freq);
      PropertyUtils.setSimpleProperty(form, "brcd", brcd);
      PropertyUtils.setSimpleProperty(form, "repclass", repclass);
      PropertyUtils.setSimpleProperty(form, "repdate", repdate);
      PropertyUtils.setSimpleProperty(form, "postdate", postdate);

      // set forwarding page base on frequency
      if (freq.equals("D"))
        actionForward = mapping.findForward("ReportListDJSP");
      else if (freq.equals("M"))
        actionForward = mapping.findForward("ReportListMJSP");
      else if (freq.equals("R"))
        actionForward = mapping.findForward("ReportListRJSP");
      else
        throw new ReportListException("Unknown frequency specified");

      // find report list
      if (freq.equals("R"))
        reportListBean.findRepListOnReq(freq, brcd, repclass, postdate);
      else
        reportListBean.findRepList(freq, brcd, repclass, repdate);

      if (log.isDebugEnabled())
      {
        log.debug("current freq      = " + reportListBean.getFreq());
        log.debug("current brcd      = " + reportListBean.getBrcd());
        log.debug("current repclass  = " + reportListBean.getRepClass());
        if (freq.equals("R"))
        {
          log.debug("current postdate  = " + reportListBean.getPostDate());
          log.debug("current postdateV = " + reportListBean.getPostDateV());
        }
        else
        {
          log.debug("current repdate   = " + reportListBean.getRepDate());
          log.debug("current repdateV  = " + reportListBean.getRepDateV());
        }

        Vector repFileV = reportListBean.getRepFileV();
        for (int i = 0; i < repFileV.size(); i++)
        {
          ReportFileBean repFileBean = (ReportFileBean)repFileV.elementAt(i);
          log.debug("report: " + i +
                    " : " + repFileBean.getCode() +
                    " : " + repFileBean.getRepDate() +
                    " : " + repFileBean.getSize() +
                    " : " + repFileBean.getName() +
                    " : " + repFileBean.getFile() +
                    " : " + repFileBean.getStatus());
        }

        if (freq.equals("R"))
          log.debug("postDateV = " + reportListBean.getPostDateV());
        else
          log.debug("repDateV  = " + reportListBean.getRepDateV());
      }

      saveReportListBean(request, reportListBean);
    }
    catch (NoSessionException e)
    {
      log.error(e);
      if (log.isDebugEnabled())
        e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("sessionTimeOut"));
      actionForward = mapping.findForward("Index");
    }
    catch (ReportListException e)
    {
      log.error(e);
      e.printStackTrace();
      saveReportListBean(request, reportListBean);
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("reportListError", e.getMessage()));
    }

    if (!actionErrors.isEmpty())
    {
      saveErrors(request, actionErrors);
    }

    log.debug("Forwarding to " + actionForward.getName());
    return actionForward;
  }

  ReportListBean getReportListBean(HttpServletRequest request, LoginBean loginBean)
    throws LoginException, ReportListException
  {
    ReportListBean reportListBean = null;

    HttpSession session = request.getSession(false);
    if (session == null)
    {
      log.debug("create new ReportListBean");
      reportListBean = new ReportListBean(loginBean);
      return reportListBean;
    }

    reportListBean = (ReportListBean)session.getAttribute("reportListBean");
    if (reportListBean == null)
    {
      log.debug("create new ReportListBean");
      reportListBean = new ReportListBean(loginBean);
      return reportListBean;
    }

    log.debug("retrieve ReportListBean from session");
    return reportListBean;
  }

  void saveReportListBean(HttpServletRequest request, ReportListBean reportListBean)
  {
    log.debug("save ReportListBean to session");
    HttpSession session = request.getSession(true);
    session.setAttribute("reportListBean", reportListBean);
  }
}
