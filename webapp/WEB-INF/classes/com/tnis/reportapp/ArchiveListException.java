package com.tnis.reportapp;

public class ArchiveListException extends Exception
{
  public ArchiveListException()
  {
    super();
  }

  public ArchiveListException(String msg)
  {
    super(msg);
  }
}
