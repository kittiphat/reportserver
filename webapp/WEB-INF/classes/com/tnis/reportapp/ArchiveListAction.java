package com.tnis.reportapp;

import java.sql.Date;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

public final class ArchiveListAction extends Action
{
  private Log log = LogFactory.getLog("com.tnis.reportapp");

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    log.debug("invoke ArchiveListAction");
    ActionErrors  actionErrors      = new ActionErrors();
    ActionForward actionForward     = null;
    ArchiveListBean archiveListBean = null;

    try
    {
      // servlet input parameters
      Integer cmd      = (Integer)PropertyUtils.getSimpleProperty(form, "cmd");
      String freq      = (String)PropertyUtils.getSimpleProperty(form, "freq");
      String brcd      = (String)PropertyUtils.getSimpleProperty(form, "brcd");
      Integer repclass = (Integer)PropertyUtils.getSimpleProperty(form, "repclass");
      Date archdate    = (Date)PropertyUtils.getSimpleProperty(form, "archdate");

      if (log.isDebugEnabled())
      {
        log.debug("input cmd      = " + cmd);
        log.debug("input freq     = " + freq);
        log.debug("input brcd     = " + brcd);
        log.debug("input repclass = " + repclass);
        log.debug("input archdate = " + archdate);
      }

      // get loginBean from session
      LoginBean loginBean = Util.getLoginBean(request);

      // create archiveListBean
      archiveListBean = getArchiveListBean(request, loginBean);

      // initial cmd parameter
      if (cmd == null) cmd = new Integer(0);

      // initial frequency
      if (freq == null || freq.equals(""))
      {
        if ((cmd.intValue() == 1) || archiveListBean.isNew())
          freq = "D";
        else
          freq = archiveListBean.getFreq();
      }

      // initial branch code
      if (brcd == null || brcd.equals(""))
      {
        if ((cmd.intValue() == 1) || archiveListBean.isNew())
        {
          brcd = loginBean.getBrcd();
          if (brcd == null || brcd.equals(""))
            brcd = (String)loginBean.getBrcdV().firstElement();
        }
        else
          brcd = archiveListBean.getBrcd();
      }

      // initial report class
      if (repclass == null)
      {
        if ((cmd.intValue() == 1) || archiveListBean.isNew())
        {
          LabelValueBean lv = (LabelValueBean)loginBean.getRepClassV().firstElement();
          repclass = new Integer(lv.getValue());
        }
        else
          repclass = archiveListBean.getRepClass();
      }

      // initial report date
      if ((archdate == null) && (cmd.intValue() != 1) && !archiveListBean.isNew())
      {
        archdate = archiveListBean.getArchDate();
      }

      // set form bean for jsp display correction
      PropertyUtils.setSimpleProperty(form, "freq", freq);
      PropertyUtils.setSimpleProperty(form, "brcd", brcd);
      PropertyUtils.setSimpleProperty(form, "repclass", repclass);
      PropertyUtils.setSimpleProperty(form, "archdate", archdate);

      // find archive list by freq, report and archdate
      archiveListBean.findArchList(freq, brcd, repclass, archdate);

      if (log.isDebugEnabled())
      {
        log.debug("current freq      = " + archiveListBean.getFreq());
        log.debug("current brcd      = " + archiveListBean.getBrcd());
        log.debug("current repclass  = " + archiveListBean.getRepClass());
        log.debug("current archdate  = " + archiveListBean.getArchDate());
        log.debug("current archdateV = " + archiveListBean.getArchDateV());

        Vector reparchV = archiveListBean.getRepArchV();
        for (int i = 0; i < reparchV.size(); i++)
        {
          ReportArchiveBean reparch = (ReportArchiveBean)reparchV.elementAt(i);
          log.debug("report: " + i +
                    " : " + reparch.getCode() +
                    " : " + reparch.getName() +
                    " : " + reparch.getStatus() +
                    " : " + reparch.getReqUid() +
                    " : " + reparch.getPermitCancel());
        }
        log.debug("archdateV = " + archiveListBean.getArchDateV());
      }

      saveArchiveListBean(request, archiveListBean);
      actionForward = mapping.findForward("ArchiveListJSP");
    }
    catch (NoSessionException e)
    {
      log.error(e);
      if (log.isDebugEnabled())
        e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("sessionTimeOut"));
      actionForward = mapping.findForward("Index");
    }
    catch (ArchiveListException e)
    {
      log.error(e);
      e.printStackTrace();
      saveArchiveListBean(request, archiveListBean);
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("archiveListError", e.getMessage()));
      actionForward = mapping.findForward("ArchiveListJSP");
    }

    if (!actionErrors.isEmpty())
    {
      saveErrors(request, actionErrors);
    }

    log.debug("Forwarding to " + actionForward.getName());
    return actionForward;
  }

  ArchiveListBean getArchiveListBean(HttpServletRequest request, LoginBean loginBean)
    throws LoginException, ArchiveListException
  {
    ArchiveListBean archiveListBean = null;

    HttpSession session = request.getSession(false);
    if (session == null)
    {
      log.debug("create new ArchiveListBean");
      archiveListBean = new ArchiveListBean(loginBean);
      return archiveListBean;
    }

    archiveListBean = (ArchiveListBean)session.getAttribute("archiveListBean");
    if (archiveListBean == null)
    {
      log.debug("create new ArchiveListBean");
      archiveListBean = new ArchiveListBean(loginBean);
      return archiveListBean;
    }

    log.debug("retrieve ArchiveListBean from session");
    return archiveListBean;
  }

  void saveArchiveListBean(HttpServletRequest request, ArchiveListBean archiveListBean)
  {
    log.debug("save ArchiveListBean to session");
    HttpSession session = request.getSession(true);
    session.setAttribute("archiveListBean", archiveListBean);
  }
}
