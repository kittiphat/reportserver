package com.tnis.reportapp;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.sql.Time;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.TreeSet;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

public class LoginBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  private String uid             = null;
  private String name            = null;
  private String uclass          = null;
  private String brcd            = null;
  private Vector brcdV           = null;
  private String brname          = null;
  private Vector repclassV       = null;
  private String ticket          = null;
  private boolean ticketbyCookie = false;
  private boolean ticketbyURL    = false;
  private int pwdfail            = 0;
  private int maxpwdfail         = 0;
  private Time last              = null;
  private Date expire            = null;
  private Date logindate         = null;
  private boolean loginflag      = false;

  public LoginBean()
  {
    log.debug("create LoginBean");
  }

  // getter methods

  public String getUid() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.uid;
  }

  public String getName() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.name;
   }

  public String getUClass() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.uclass;
  }

  public String getBrcd() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.brcd;
  }

  public Vector getBrcdV() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.brcdV;
  }

  public String getBrName() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.brname;
  }

  public Vector getRepClassV() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.repclassV;
  }

  public String getTicket() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.ticket;
  }

  public boolean getTicketByCookie() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.ticketbyCookie;
  }

  public boolean getTicketByURL() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.ticketbyURL;
  }

  public int getMaxPwdFail() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.maxpwdfail;
  }

  public Time getLast() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.last;
  }

  public Date getExpire() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.expire;
  }

  public Date getLoginDate() throws LoginException
  {
    if (this.loginflag == false)
      throw new LoginException("User does not login");

    return this.logindate;
  }

  public boolean isExpire()
  {
    GregorianCalendar logincalendar  = new GregorianCalendar();
    GregorianCalendar expirecalendar = new GregorianCalendar();

    logincalendar.setTime(this.logindate);
    logincalendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
    logincalendar.set(GregorianCalendar.MINUTE, 0);
    logincalendar.set(GregorianCalendar.SECOND, 0);
    logincalendar.set(GregorianCalendar.MILLISECOND, 0);

    expirecalendar.setTime(this.expire);
    expirecalendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
    expirecalendar.set(GregorianCalendar.MINUTE, 0);
    expirecalendar.set(GregorianCalendar.SECOND, 0);
    expirecalendar.set(GregorianCalendar.MILLISECOND, 0);

    return logincalendar.after(expirecalendar);
  }

  public void Login(String in_uid, String in_passwd)
    throws LoginException, IOException, NamingException, NoSuchAlgorithmException, SQLException
  {
    Connection conn     = null;
    Statement stmt      = null;
    ResultSet rs        = null;
    String passwd_md5   = null;
    TreeSet brcdS       = null;

    log.debug("user [" + in_uid + "] try to login with password [" + in_passwd + "]");
    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();
      log.debug("select uid,name,passwd,class,brcd from user where uid = '" + in_uid + "'");
      rs = stmt.executeQuery("select uid,name,passwd,class,brcd,pwdfail,last,expire from user where uid = '" + in_uid + "'");
      if (!rs.next())
        throw new LoginException("User " + in_uid + " does not exist");

      log.debug("found user " + in_uid);
      this.uid     = rs.getString("uid");
      this.name    = rs.getString("name");
      passwd_md5   = rs.getString("passwd");
      this.uclass  = rs.getString("class");
      this.brcd    = rs.getString("brcd");
      this.pwdfail = rs.getInt("pwdfail");
      this.last    = rs.getTime("last");
      this.expire  = rs.getDate("expire");

      // find current date
      java.util.Date date = new java.util.Date();
      this.logindate = new Date(date.getTime());
      log.debug("logindate = " + this.logindate);

      // check user expiration date
      //if (isExpire())
      //  throw new LoginException("User " + this.uid + " has expired");

      // read maximum password fail counter
      rs = stmt.executeQuery("select varvalue from preferences where varname = 'MAX_PWDFAIL'");
      while (rs.next())
        this.maxpwdfail = rs.getInt("varvalue");

      // verify invalid password counter
      if (this.pwdfail >= this.maxpwdfail)
        throw new LoginException("User " + this.uid + " locked (max password failed)");

      // verify password
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      String in_passwd_md5 = toHexString(md5.digest(in_passwd.getBytes()));
      if (!in_passwd_md5.equals(passwd_md5))
      {
        stmt.executeUpdate("update user set pwdfail = pwdfail + 1 where uid = '" + this.uid + "'");
        throw new LoginException("Invalid password");
      }

      // create brcd list and add first entry from user's brcd
      //this.brcdV = new Vector();
      brcdS = new TreeSet(new BrcdComparator());

      if (!this.brcd.equals(""))
      {
        // verify valid brcd
        rs = stmt.executeQuery("select descr from brcd where brcd = '" + brcd + "'");
        if (!rs.next())
          throw new LoginException("User has invalid brcd [" + brcd + "]");

        brname = rs.getString("descr");
        //this.brcdV.addElement(this.brcd);
        brcdS.add(this.brcd);
      }

      // find brcd list
      rs = stmt.executeQuery("(select brcd from brcd_acl where " +
                             "obj = '" + this.uid + "' and " +
                             "type = 'U' and brcd = '*' and " +
                             "expire >= '" + this.logindate.toString() + "') " +
                             "union " +
                             "(select brcd from brcd_acl where " +
                             "obj = '" + this.uclass + "' and " +
                             "type = 'UC' and brcd = '*' and " +
                             "expire >= '" + this.logindate.toString() + "') " +
                             "order by binary brcd");
      if (rs.next())
      {
        // add all branch, put user brcd on top most
        rs = stmt.executeQuery("select brcd from brcd where brcd <> '" + this.brcd + "' order by binary brcd");
        while (rs.next())
          //this.brcdV.addElement(rs.getString("brcd"));
          brcdS.add(rs.getString("brcd"));
      }
      else
      {
        // add branch from brcd_acl
        rs = stmt.executeQuery("(select distinct brcd from brcd_acl where " +
                               "obj = '" + this.uid + "' and type = 'U' and " +
                               "brcd <> '" + this.brcd + "' and " +
                               "expire >= '" + this.logindate.toString() + "') " +
                               "union distinct " +
                               "(select distinct brcd from brcd_acl where " +
                               "obj = '" + this.uclass + "' and type = 'UC' and " +
                               "brcd <> '" + this.brcd + "' and " +
                               "expire >= '" + this.logindate.toString() + "') " +
                               "order by binary brcd");
        while (rs.next())
          //this.brcdV.addElement(rs.getString("brcd"));
          brcdS.add(rs.getString("brcd"));
      }

      if (brcdS.isEmpty())
      {
        throw new LoginException("User " + uid + " does not has permission on any branch");
      }

      // find additional branches from region mapping
      rs = stmt.executeQuery("(select use_regmap from regmap_acl where " +
                             "obj = '" + this.uid + "' and type = 'U' and " +
                             "expire >= '" + this.logindate.toString() + "') " +
                             "union distinct " +
                             "(select use_regmap from regmap_acl where " +
                             "obj = '" + this.uclass + "' and type = 'UC' and " +
                             "expire >= '" + this.logindate.toString() + "')");
      if (rs.next())
      {
        String use_regmap = rs.getString("use_regmap");
        if (use_regmap.equals("Y") && !this.brcd.equals(""))
        {
          log.debug("use_regmap = 'Y', find additional brcdV");
          rs = stmt.executeQuery("select brcd from regmap where reg = '" + this.brcd + "'");
          while (rs.next())
            //this.brcdV.addElement(rs.getString("brcd"));
            brcdS.add(rs.getString("brcd"));
        }
      }

      // set brcdS to brcdV
      this.brcdV = new Vector(brcdS);

      // find repclass list
      this.repclassV = new Vector();

      // select distinct class from repclass_acl
      rs = stmt.executeQuery("(select distinct rca.class,rc.descr from " +
                             "repclass_acl as rca join repclass rc " +
                             "on (rca.class = rc.class) where " +
                             "rca.obj = '" + this.uid + "' and " +
                             "rca.type = 'U' and " +
                             "rca.expire >= '" + logindate.toString() + "') " +
                             "union distinct " +
                             "(select distinct rca.class,rc.descr from " +
                             "repclass_acl as rca join repclass rc " +
                             "on (rca.class = rc.class) where " +
                             "rca.obj = '" + this.uclass + "' and " +
                             "rca.type = 'UC' and " +
                             "rca.expire >= '" + logindate.toString() + "') " +
                             "order by class");
      while (rs.next())
        this.repclassV.addElement(new LabelValueBean(rs.getString("descr"), rs.getString("class")));

      if (this.repclassV.isEmpty())
        throw new LoginException("User " + uid + " does not has permission on any report class");

      // create login ticket
      this.ticket = randTicket(32);
      log.debug("login ticket [" + this.ticket + "] created");

      // check flag for send ticket by cookie
      rs = stmt.executeQuery("select varvalue from preferences where varname = 'TICKET_BY_COOKIE'");
      while (rs.next())
      {
        if (rs.getString("varvalue").equals("Y"))
          this.ticketbyCookie = true;
      }

      // check flag for send ticket by url
      rs = stmt.executeQuery("select varvalue from preferences where varname = 'TICKET_BY_URL'");
      while (rs.next())
      {
        if (rs.getString("varvalue").equals("Y"))
          this.ticketbyURL = true;
      }

      // reset pwdfail counter and save login timestamp
      stmt.executeUpdate("update user set pwdfail = 0 where uid = '" + this.uid + "'");
      Time logintime = new Time(logindate.getTime());
      String laststr = logindate.toString() + " " + logintime.toString();
      log.debug("laststr = " + laststr);
      stmt.executeUpdate("update user set last = '" + laststr + "' where uid = '" + this.uid + "'");

      this.loginflag = true;
      log.debug("login success");
    }
    catch (LoginException e)
    {
      Logout();
      throw e;
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }

  public void Logout()
  {
    this.loginflag = false;
    this.uid       = null;
    this.name      = null;
    this.uclass    = null;
    this.brcd      = null;
    this.brcdV     = null;
    this.repclassV = null;
    this.ticket    = null;
    this.logindate = null;
  }

  private String toHexString(byte[] digest)
  {
    char[] values={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
    StringBuffer hex=new StringBuffer();
    for(int i = 0; i < digest.length; i++)
    {
      byte b = digest[i];
      hex.append(values[(b >> 4) & 0xf]);
      hex.append(values[b & 0x0f]);
    }
    return hex.toString();
  }

  private String randTicket(int length)
  {
    char[] chars  = new char[36];
    char[] ticket = new char[length];
    Random rand   = new Random();

    // initialize character set
    for (int i = 0; i < 26; i++) chars[i]      = (char)(i + 65);
    for (int i = 0; i < 10; i++) chars[i + 26] = (char)(i + 48);

    // generate random string
    for (int i = 0; i < length; i++)
    {
      ticket[i] = chars[rand.nextInt(chars.length)];
    }

    return new String(ticket);
  }
}
