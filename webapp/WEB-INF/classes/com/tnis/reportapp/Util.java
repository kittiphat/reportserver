package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

public class Util
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");
  static String dsname = "jdbc/report";

  public static Connection createConnection()
    throws NamingException, IOException, SQLException
  {
    log.debug("Create DB connection to " + dsname);

    InitialContext ctx = new InitialContext();
    Context envCtx     = (Context)ctx.lookup("java:comp/env");
    DataSource ds      = (DataSource)envCtx.lookup(dsname);
    Connection conn    = ds.getConnection();
    return conn;
  }

  public static void clearSession(HttpSession session)
  {
    Enumeration attribEnum = session.getAttributeNames();
    if (attribEnum == null)
    while (attribEnum.hasMoreElements())
    {
      session.removeAttribute((String)attribEnum.nextElement());
    }
  }

  public static LoginBean getLoginBean(HttpServletRequest request)
    throws NoSessionException
  {
    HttpSession session = request.getSession(false);
    if (session == null)
    {
      throw new NoSessionException("User not login or session timeout");
    }

    LoginBean loginBean = (LoginBean)session.getAttribute("loginBean");
    if (loginBean == null)
    {
      throw new NoSessionException("User not login or session timeout");
    }

    return loginBean;
  }

  public static boolean checkBrcdPermission(Vector brcdV, String brcd)
  {
    return brcdV.contains(brcd);
  }

  public static boolean checkRepClassPermission(Vector repclassV, Integer repclass)
  {
    boolean found = false;

    for (int i = 0; i < repclassV.size(); i++)
    {
      LabelValueBean lv = (LabelValueBean)repclassV.elementAt(i);
      Integer repclass1  = new Integer(lv.getValue());
      if (repclass1.equals(repclass))
        found = true;
    }
    return found;
  }
}
