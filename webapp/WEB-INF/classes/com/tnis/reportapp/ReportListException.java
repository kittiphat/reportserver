package com.tnis.reportapp;

public class ReportListException extends Exception
{
  public ReportListException()
  {
    super();
  }

  public ReportListException(String msg)
  {
    super(msg);
  }
}
