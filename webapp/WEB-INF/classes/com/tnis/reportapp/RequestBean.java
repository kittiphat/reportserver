package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RequestBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  // initialize in constructor
  private String uid        = null;
  private Vector brcdV      = null;
  private Vector repclassV  = null;
  private Date   logindate  = null;

  public RequestBean(LoginBean loginBean) throws LoginException
  {
    if (log.isDebugEnabled())
    {
      log.debug("create RequestBean");
      log.debug("uid       = " + loginBean.getUid());
      log.debug("brcdV     = " + loginBean.getBrcdV());
      log.debug("repclassV = " + loginBean.getRepClassV());
      log.debug("logindate = " + loginBean.getLoginDate());
    }
    this.uid       = loginBean.getUid();
    this.brcdV     = loginBean.getBrcdV();
    this.repclassV = loginBean.getRepClassV();
    this.logindate = loginBean.getLoginDate();
  }
  
  public void sendRequest(String code, String freq, String brcd, Date repdate)
    throws RequestException, IOException, NamingException, SQLException
  {
    Connection conn         = null;
    Statement stmt          = null;
    ResultSet rs            = null;
    Integer repclass        = null;

    if (log.isDebugEnabled())
    {
      log.debug("invoke sendRequest(code, freq, repdate)");
      log.debug("code    = " + code);
      log.debug("freq    = " + freq);
      log.debug("brcd    = " + brcd);
      log.debug("repdate = " + repdate);
    }

    // check branch permission
    if (!Util.checkBrcdPermission(this.brcdV, brcd))
    {
      throw new RequestException("No permission on branch " + brcd);
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      // check report class permission
      rs = stmt.executeQuery("select class from repcode where code = '" + code + "'");
      if (!rs.next())
      {
        throw new RequestException("Invalid report code: " + code);
      }

      repclass = new Integer(rs.getInt("class"));
      log.debug("repclassV = " + repclassV);
      log.debug("repclass  = " + repclass);
      if (repclass == null)
      {
        throw new RequestException("Report " + code + " has undefined report class");
      }

      if (!Util.checkRepClassPermission(this.repclassV, repclass))
      {
        throw new RequestException("No permission on report " + code);
      }

      // find report from reparchive
      rs = stmt.executeQuery("select code from reparchive " +
                             "where code = '" + code +
                             "' and freq = '" + freq +
                             "' and brcd = '" + brcd +
                             "' and repdate = '" + repdate + "'");
      if (!rs.next())
      {
        throw new RequestException("Report archive not found");
      }

      // insert request record to repreq
      log.debug("insert into repreq (code,freq,brcd,repdate,requid,reqdate) " +
                "values ('" + code + "','" + freq + "','" + brcd +
                "','" + repdate + "','" + this.uid + "','" + this.logindate + "')");
      stmt.executeUpdate("insert into repreq (code,freq,brcd,repdate,requid,reqdate) " +
                         "values ('" + code + "','" + freq + "','" + brcd +
                         "','" + repdate + "','" + this.uid + "','" + this.logindate + "')");
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }
}
