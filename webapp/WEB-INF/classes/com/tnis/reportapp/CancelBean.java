package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CancelBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  // initialize in constructor
  private String uid   = null;
  private Vector brcdV = null;

  public CancelBean(LoginBean loginBean) throws LoginException
  {
    if (log.isDebugEnabled())
    {
      log.debug("create CancelBean");
      log.debug("uid   = " + loginBean.getUid());
      log.debug("brcdV = " + loginBean.getBrcdV());
    }
    this.uid   = loginBean.getUid();
    this.brcdV = loginBean.getBrcdV();
  }
  
  public void sendCancel(String code, String freq, String brcd, Date repdate)
    throws CancelException, IOException, NamingException, SQLException
  {
    Connection conn         = null;
    Statement stmt          = null;

    if (log.isDebugEnabled())
    {
      log.debug("invoke sendCancel(code, freq, repdate)");
      log.debug("code    = " + code);
      log.debug("freq    = " + freq);
      log.debug("brcd    = " + brcd);
      log.debug("repdate = " + repdate);
    }

    // check branch permission
    if (!Util.checkBrcdPermission(this.brcdV, brcd))
    {
      throw new CancelException("No permission on branch " + brcd);
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      // delete request record from repreq
      int res = stmt.executeUpdate("delete from repreq " +
                                   "where code = '" + code +
                                   "' and freq = '" + freq +
                                   "' and brcd = '" + brcd +
                                   "' and repdate = '" + repdate +
                                   "' and requid = '" + uid + "'");
      if (res == 0)
        throw new CancelException("Request not found");
    }
    finally
    {
      log.debug("closing DB connection");
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }
}
