package com.tnis.reportapp;

import java.util.Comparator;

public class BrcdComparator implements Comparator
{
  public BrcdComparator() {}

  public int compare(Object o1, Object o2)
  {
    String s1      = o1.toString();
    String s2      = o2.toString();
    Integer i1     = null;
    Integer i2     = null;
    boolean is_int = false;

    try
    {
      i1     = new Integer(s1);
      i2     = new Integer(s2);
      is_int = true;
    }
    catch (NumberFormatException e) {}

    if (is_int) return i1.compareTo(i2);
    else        return s1.compareTo(s2);
  }
} 
