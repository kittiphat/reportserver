package com.tnis.reportapp;

public class NoSessionException extends Exception
{
  public NoSessionException()
  {
    super();
  }

  public NoSessionException(String msg)
  {
    super(msg);
  }
}
