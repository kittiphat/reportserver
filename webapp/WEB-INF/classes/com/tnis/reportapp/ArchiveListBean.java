package com.tnis.reportapp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ArchiveListBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  // initialize in constructor
  private String uid        = null;
  private Vector brcdV      = null;
  private Vector repclassV  = null;

  // change by findRepList methods
  private boolean isnew     = true;
  private String  freq      = null;
  private String  brcd      = null;
  private Integer repclass  = null;
  private Date    archdate  = null;
  private Vector  archdateV = null;
  private Vector  reparchV  = null;

  public ArchiveListBean(LoginBean loginBean)
    throws ArchiveListException, LoginException
  {
    if (log.isDebugEnabled())
    {
      log.debug("create ArchiveListBean");
      log.debug("uid       = " + loginBean.getUid());
      log.debug("brcdV     = " + loginBean.getBrcdV());
      log.debug("repclassV = " + loginBean.getRepClassV());
    }
    this.uid       = loginBean.getUid();
    this.brcdV     = loginBean.getBrcdV();
    this.repclassV = loginBean.getRepClassV();
  }

  // getter methods
  public boolean isNew()
  {
    return this.isnew;
  }

  public String getFreq() throws ArchiveListException
  {
    if (this.freq == null)
    {
      throw new ArchiveListException("freq not initialized");
    }
    return this.freq;
  }

  public String getBrcd() throws ArchiveListException
  {
    if (this.brcd == null)
    {
      throw new ArchiveListException("brcd not initialized");
    }
    return this.brcd;
  }

  public Integer getRepClass() throws ArchiveListException
  {
    if (this.repclass == null)
    {
      throw new ArchiveListException("repclass not initialized");
    }
    return this.repclass;
  }

  public Date getArchDate() throws ArchiveListException
  {
    if (this.archdate == null)
    {
      throw new ArchiveListException("archdate not initialized");
    }
    return this.archdate;
  }

  public Vector getArchDateV() throws ArchiveListException
  {
    if (this.archdateV == null)
    {
      throw new ArchiveListException("archdateV not initialized");
    }
    return this.archdateV;
  }

  public Vector getRepArchV() throws ArchiveListException
  {
    if (this.reparchV == null)
    {
      throw new ArchiveListException("reparchV not initialized");
    }
    return reparchV;
  }

  public void findArchList(String freq, String brcd, Integer repclass, Date archdate)
    throws ArchiveListException, IOException, NamingException, SQLException
  {
    Connection conn    = null;
    Statement stmt     = null;
    ResultSet rs       = null;
    Vector archdateV1  = new Vector();
    Vector reparchV1   = new Vector(100);
    Hashtable reparchH = new Hashtable(100);

    if (log.isDebugEnabled())
    {
      log.debug("invoke findArchList(freq, brcd, repclass, archdate)");
      log.debug("freq     = " + freq);
      log.debug("brcd     = " + brcd);
      log.debug("repclass = " + repclass);
      log.debug("archdate = " + archdate);
    }

    // initialize private variables
    this.isnew     = false;
    this.freq      = freq;
    this.brcd      = brcd;
    this.repclass  = repclass;
    this.archdate  = new Date(0);
    this.archdateV = new Vector();
    this.reparchV  = new Vector();

    // check branch permission
    if (!Util.checkBrcdPermission(this.brcdV, brcd))
    {
      throw new ArchiveListException("No permission on branch " + brcd);
    }

    // check report class permission
    if (!Util.checkRepClassPermission(this.repclassV, repclass))
    {
      throw new ArchiveListException("No permission on report class " + repclass);
    }

    // if not specify archdate, find report date by frequency and select latest date
    if (archdate == null)
    {
      this.archdateV = findArchDateByFreq(freq);
      this.archdate  = (Date)archdateV.firstElement();
    }
    else
    {
      archdateV1     = findArchDateByFreq(freq);
      this.archdate  = archdate;
      if (!archdateV1.contains(archdate))
      {
        throw new ArchiveListException("No report on " + archdate.toString());
      }
    }

    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      // 1st round, create master archive list from repcode
      log.debug("create master archive list from repcode");
      rs = stmt.executeQuery("select code,name from repcode " +
                             "where class = '" + this.repclass + "' and " +
                             "freq = '" + this.freq + "' order by code");
      while (rs.next())
      {
        String repcode = rs.getString("code");
        ReportArchiveBean reparch = new ReportArchiveBean();
        reparch.setCode(repcode);
        reparch.setName(rs.getString("name"));
        reparch.setStatus(ReportArchiveBean.NO_DATA);
        reparch.setPermitCancel(false);
        reparchV1.addElement(reparch);
        reparchH.put(repcode, new Integer(reparchV1.size() - 1));
      }

      // 2nd round, find on-tape report from reparchive
      log.debug("find on tape report from reparchive");
      rs = stmt.executeQuery("select rc.code " +
                             "from repcode as rc join reparchive as ra using (code,freq) " +
                             "where rc.class = '" + this.repclass + "' and " +
                             "rc.freq = '" + this.freq + "' and " +
                             "ra.brcd = '" + this.brcd + "' and " +
                             "ra.repdate = '" + this.archdate + "'");

      while (rs.next())
      {
        Integer i = (Integer)reparchH.get(rs.getString("code"));
        if (i == null) continue;

        ReportArchiveBean reparch = (ReportArchiveBean)reparchV1.elementAt(i.intValue());
        reparch.setStatus(ReportArchiveBean.ON_TAPE);

        reparchV1.setElementAt(reparch, i.intValue());
      }

      // 3rd round, find out requested report
      log.debug("find out requested report");
      rs = stmt.executeQuery("select rc.code,rr.requid " +
                             "from repcode as rc join repreq as rr using (code,freq) " +
                             "where rc.class = " + this.repclass + " and " +
                             "rc.freq = '" + this.freq + "' and " +
                             "rr.brcd = '" + this.brcd + "' and " +
                             "rr.repdate = '" + this.archdate + "'");
      while (rs.next())
      {
        Integer i = (Integer)reparchH.get(rs.getString("code"));
        if (i == null) continue;

        String requid = rs.getString("requid");
        ReportArchiveBean reparch = (ReportArchiveBean)reparchV1.elementAt(i.intValue());
        reparch.setStatus(ReportArchiveBean.REQUESTED);
        reparch.setReqUid(requid);

        // permit to cancel request for the same user id
        if (requid.equals(this.uid))
          reparch.setPermitCancel(true);

        reparchV1.setElementAt(reparch, i.intValue());
      }

      // 4th round, find out online report
      log.debug("find out online report");
      rs = stmt.executeQuery("select distinct rc.code,rc.class,rf.freq,rf.postdate " +
                             "from repcode as rc join repfile as rf using (code,freq) " +
                             "where rc.class = " + this.repclass + " and " +
                             "rf.brcd = '" + this.brcd + "' and " +
                             "rf.repdate = '" + this.archdate + "'");
      while (rs.next())
      {
        Integer i = (Integer)reparchH.get(rs.getString("code"));
        if (i == null) continue;

        ReportArchiveBean reparch = (ReportArchiveBean)reparchV1.elementAt(i.intValue());
        reparch.setFreq(rs.getString("freq"));
        reparch.setRepClass(rs.getString("class"));
        reparch.setPostDate(rs.getDate("postdate"));
        reparch.setStatus(ReportArchiveBean.ONLINE);
        reparchV1.setElementAt(reparch, i.intValue());
      }

      this.reparchV = reparchV1;

      if (archdate != null)
      {
        this.archdateV = archdateV1;
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }
  }

  private Vector findArchDateByFreq(String freq)
    throws ArchiveListException, IOException, NamingException, SQLException
  {
    Connection conn   = null;
    Statement stmt    = null;
    ResultSet rs      = null;
    Vector archdateV1 = new Vector();

    log.debug("invoke findArchDateByFreq");
    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();
      log.debug("select date from archdate where freq = '" + freq + "' order by date desc");
      rs = stmt.executeQuery("select date from archdate where freq = '" + freq + "' order by date desc");
      while (rs.next())
      {
        archdateV1.addElement(rs.getDate("date"));
      }
      if (archdateV1.isEmpty())
      {
        throw new ArchiveListException("No report on given frequency");
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }

    return archdateV1;
  }
}
