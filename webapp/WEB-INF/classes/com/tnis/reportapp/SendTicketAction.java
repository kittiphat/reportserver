package com.tnis.reportapp;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;

public final class SendTicketAction extends Action
{
  private Log log = LogFactory.getLog("com.tnis.reportapp");

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    String ticket = null;

    log.debug("invoke SendTicketAction");
    ActionErrors  actionErrors  = new ActionErrors();
    ActionForward actionForward = null;

    try
    {
      // try get Ticket from browser's cookie
      ticket = retrieveTicket(request);

      // if not found ticket from cookie, try from URL parameter
      if (ticket == null)
      {
        ticket = (String)PropertyUtils.getSimpleProperty(form, "LoginTicket");
        log.debug("got ticket from URL = [" + ticket + "]");
      }

      // get LoginBean from ServletContext
      LoginBean loginBean = retrieveLoginBean(ticket);
      log.debug("found loginBean for uid = [" + loginBean.getUid() + "]");

      // create HttpSession
      log.debug("create HttpSession and embedding loginBean");
      HttpSession session = request.getSession();
      session.invalidate();
      session = request.getSession();
      session.setAttribute("loginBean", loginBean);

      actionForward = mapping.findForward("ReportListRedir");
    }
    catch (TicketException e)
    {
      log.error(e);
      e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("ticketError", e.getMessage()));
      actionForward = mapping.findForward("Index");
    }

    log.debug("Forwarding to " + actionForward.getName());
    return actionForward;
  }

  private String retrieveTicket(HttpServletRequest request)
  {
    Cookie cookie[] = null;
    String ticket   = null;

    cookie = request.getCookies();
    if (cookie == null)
    {
      log.debug("cannot get cookie from HttpRequest");
      return null;
    }

    log.debug("retrieving Ticket from cookie");
    for (int i = 0; i < cookie.length; i++)
    {
      if (cookie[i].getName().equals("LoginTicket"))
      {
        ticket = cookie[i].getValue();
      }
    }

    return ticket;
  }

  private LoginBean retrieveLoginBean(String ticket) throws TicketException
  {
    ActionServlet servlet     = this.getServlet();
    ServletConfig servletcfg  = servlet.getServletConfig();
    ServletContext servletctx = servletcfg.getServletContext();
    LoginBean loginBean       = (LoginBean)servletctx.getAttribute(ticket);

    if (loginBean == null)
    {
      throw new TicketException("Cannot find LoginBean from ServletContext");
    }

    servletctx.removeAttribute(ticket);
    return loginBean;
  }
}
