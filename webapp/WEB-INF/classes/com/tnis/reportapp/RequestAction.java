package com.tnis.reportapp;

import java.sql.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public final class RequestAction extends Action
{
  private Log log = LogFactory.getLog("com.tnis.reportapp");

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    log.debug("invoke RequestAction");
    ActionErrors  actionErrors  = new ActionErrors();
    ActionForward actionForward = null;

    try
    {
      // servlet input parameters
      String code  = (String)PropertyUtils.getSimpleProperty(form, "code");
      String freq  = (String)PropertyUtils.getSimpleProperty(form, "freq");
      String brcd  = (String)PropertyUtils.getSimpleProperty(form, "brcd");
      Date repdate = (Date)PropertyUtils.getSimpleProperty(form, "repdate");

      if (log.isDebugEnabled())
      {
        log.debug("input code    = " + code);
        log.debug("input freq    = " + freq);
        log.debug("input brcd    = " + brcd);
        log.debug("input repdate = " + repdate);
      }

      if (code == null || code.equals("") ||
          freq == null || freq.equals("") ||
          brcd == null || brcd.equals("") ||
          repdate == null)
      {
        throw new RequestException("Incomplete input parameter");
      }

      // get loginBean from session
      LoginBean loginBean = Util.getLoginBean(request);

      // create requestBean and send a request
      RequestBean requestBean = new RequestBean(loginBean);
      requestBean.sendRequest(code, freq, brcd, repdate);

      actionForward = mapping.findForward("ArchiveListRedir");
    }
    catch (NoSessionException e)
    {
      log.error(e);
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("sessionTimeOut"));
      actionForward = mapping.findForward("Index");
    }
    catch (RequestException e)
    {
      log.error(e);
      e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("archiveListError", e.getMessage()));
      actionForward = mapping.findForward("ArchiveListRedir");
    }

    if (!actionErrors.isEmpty())
    {
      saveErrors(request, actionErrors);
    }

    log.debug("Forwarding to " + actionForward.getName());
    return actionForward;
  }
}
