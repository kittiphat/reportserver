package com.tnis.reportapp;

import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.RequestUtils;

public final class LoginAction extends Action
{
  private Log log = LogFactory.getLog("com.tnis.reportapp");

  public ActionForward execute(ActionMapping mapping,
                               ActionForm form,
                               HttpServletRequest request,
                               HttpServletResponse response)
    throws Exception
  {
    log.debug("invoke LoginAction");
    ActionErrors  actionErrors  = new ActionErrors();
    ActionForward actionForward = null;

    try
    {
      // servlet input parameters
      String uid    = (String)PropertyUtils.getSimpleProperty(form, "uid");
      String passwd = (String)PropertyUtils.getSimpleProperty(form, "passwd");

      log.info("user [" + uid + "] try to login from [" + request.getRemoteAddr() + "]");
      LoginBean loginBean = new LoginBean();
      loginBean.Login(uid, passwd);

      if (log.isDebugEnabled())
      {
        log.debug("UID       = " + loginBean.getUid());
        log.debug("Name      = " + loginBean.getName());
        log.debug("Class     = " + loginBean.getUClass());
        log.debug("BRCD      = " + loginBean.getBrcdV().toString());
        log.debug("RepClass  = " + loginBean.getRepClassV().toString());
        log.debug("Ticket    = " + loginBean.getTicket());
        log.debug("LoginDate = " + loginBean.getLoginDate());
      }

      // embedding loginBean to ServletContext
      log.debug("embedding loginBean to ServletContext, indexed by Ticket");
      embedLoginBean(loginBean.getTicket(), loginBean);

      // add Ticket to browser's cookie
      if (loginBean.getTicketByCookie())
      {
        Cookie cookie = new Cookie("LoginTicket", loginBean.getTicket());
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
      }

      // find send-ticket URL for switch from SSL (login page) to non-SSL
      String sendticketURL = findSendTicketURL(request, mapping, "SendTicket");
      if (loginBean.getTicketByURL())
        sendticketURL = sendticketURL + "?LoginTicket=" + loginBean.getTicket();
      request.setAttribute("SendTicketURL", sendticketURL);
      log.debug("SendTicketURL = " + sendticketURL);

      actionForward = mapping.findForward("LoginJSP");
    }
    catch (MalformedURLException e)
    {
      log.error(e);
      e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("loginError", e.getMessage()));
      actionForward = mapping.findForward("Index");
    }
    catch (LoginException e)
    {
      log.error(e);
      if (log.isDebugEnabled())
        e.printStackTrace();
      actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("loginError", e.getMessage()));
      actionForward = mapping.findForward("Index");
    }

    if (!actionErrors.isEmpty())
    {
      saveErrors(request, actionErrors);
    }

    log.debug("Forwarding to " + actionForward.getName());
    return actionForward;
  }

  private void embedLoginBean(String ticket, LoginBean loginBean)
  {
    ActionServlet servlet     = this.getServlet();
    ServletConfig servletcfg  = servlet.getServletConfig();
    ServletContext servletctx = servletcfg.getServletContext();
    servletctx.setAttribute(ticket, loginBean);
  }

  private String findSendTicketURL(HttpServletRequest request, ActionMapping mapping, String forwardName)
    throws MalformedURLException
  {
    URL loginURL          = null;
    URL replistURL        = null;
    ActionForward forward = null;

    loginURL   = RequestUtils.serverURL(request);
    forward    = mapping.findForward(forwardName);
    replistURL = new URL("http", loginURL.getHost(), -1, request.getContextPath() + forward.getPath());

    return replistURL.toString();
  }
}
