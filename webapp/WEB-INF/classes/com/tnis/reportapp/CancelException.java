package com.tnis.reportapp;

public class CancelException extends Exception
{
  public CancelException()
  {
    super();
  }

  public CancelException(String msg)
  {
    super(msg);
  }
}
