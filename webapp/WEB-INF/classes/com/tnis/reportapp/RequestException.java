package com.tnis.reportapp;

public class RequestException extends Exception
{
  public RequestException()
  {
    super();
  }

  public RequestException(String msg)
  {
    super(msg);
  }
}
