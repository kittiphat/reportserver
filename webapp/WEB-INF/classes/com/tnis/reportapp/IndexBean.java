package com.tnis.reportapp;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.RequestUtils;

public class IndexBean
{
  static Log log = LogFactory.getLog("com.tnis.reportapp");

  private String indexMsg1 = "";
  private String indexMsg2 = "";
  private String indexMsg3 = "";
  private String loginURL  = null;

  public IndexBean(HttpServletRequest request)
    throws IOException, NamingException, SQLException
  {
    Connection conn   = null;
    Statement stmt    = null;
    ResultSet rs      = null;
    URL url           = null;
    URL loginurl      = null;
    boolean ssl_logon = false;

    log.debug("create IndexBean");
    try
    {
      conn = Util.createConnection();
      stmt = conn.createStatement();

      rs = stmt.executeQuery("select varvalue from preferences where varname = 'INDEX_MSG1'");
      if (rs.next())
      {
        this.indexMsg1 = rs.getString("varvalue");
      }

      rs = stmt.executeQuery("select varvalue from preferences where varname = 'INDEX_MSG2'");
      if (rs.next())
      {
        this.indexMsg2 = rs.getString("varvalue");
      }

      rs = stmt.executeQuery("select varvalue from preferences where varname = 'INDEX_MSG3'");
      if (rs.next())
      {
        this.indexMsg3 = rs.getString("varvalue");
      }

      rs = stmt.executeQuery("select varvalue from preferences where varname = 'SSL_LOGON'");
      if (rs.next())
      {
        if (rs.getString("varvalue").equals("Y"))
          ssl_logon = true;
      }
    }
    finally
    {
      log.debug("closing DB connection");
      if (rs   != null) try { rs.close();   rs   = null; } catch (Exception e) {}
      if (stmt != null) try { stmt.close(); stmt = null; } catch (Exception e) {}
      if (conn != null) try { conn.close(); conn = null; } catch (Exception e) {}
    }

    url = RequestUtils.serverURL(request);
    if (ssl_logon)
      loginurl = new URL("https", url.getHost(), -1, request.getContextPath() + "/Login.do");
    else
      loginurl = new URL("http", url.getHost(), -1, request.getContextPath() + "/Login.do");

    this.loginURL = loginurl.toString();
    log.debug("loginURL = " + this.loginURL);
  }

  // getter methods

  public String getIndexMsg1()
  {
    return this.indexMsg1;
  }

  public String getIndexMsg2()
  {
    return this.indexMsg2;
  }

  public String getIndexMsg3()
  {
    return this.indexMsg3;
  }

  public String getLoginURL()
  {
    return this.loginURL;
  }
}
