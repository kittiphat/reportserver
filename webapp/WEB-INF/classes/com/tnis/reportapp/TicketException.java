package com.tnis.reportapp;

public class TicketException extends Exception
{
  public TicketException()
  {
    super();
  }

  public TicketException(String msg)
  {
    super(msg);
  }
}
