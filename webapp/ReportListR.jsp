<%@ page contentType="text/html; charset=TIS-620" %>
<%@ page language="java" %>
<%@ page import="java.util.*, org.apache.struts.util.LabelValueBean" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<jsp:useBean id="loginBean"      scope="session" type="com.tnis.reportapp.LoginBean"     />
<jsp:useBean id="reportListBean" scope="session" type="com.tnis.reportapp.ReportListBean"/>
<bean:define id="repClassV" name="loginBean"      property="repClassV"/>
<bean:define id="repFileV"  name="reportListBean" property="repFileV" />

<%
  Vector postdateV = reportListBean.getPostDateV();
  Vector dateBeanV = new Vector();
  for (int i = 0; i < postdateV.size(); i++)
  {
    dateBeanV.add(new LabelValueBean(postdateV.elementAt(i).toString(), postdateV.elementAt(i).toString()));
  }
  pageContext.setAttribute("dateBeanV", dateBeanV);

  Vector brcdV = loginBean.getBrcdV();
  Vector brcdBeanV = new Vector();
  for (int i = 0; i < brcdV.size(); i++)
  {
    brcdBeanV.add(new LabelValueBean(brcdV.elementAt(i).toString(), brcdV.elementAt(i).toString()));
  }
  pageContext.setAttribute("brcdBeanV", brcdBeanV);
%>

<html:html>

<head>
<jsp:include page='html_head.html'/>
<jsp:include page='javascript.html'/>
</head>

<body text=#FFFFFF bgColor=#1E3DAA leftMargin=12 topMargin=12 marginHeight="0" marginWidth="0"
 link="#FF9933" vlink="#6699FF" alink="#6699FF">

<table cellSpacing="0" cellPadding="0" height="94" width="771" border="0">
  <tbody> 
  <tr> 
    <td vAlign=top height="87" width="257"><html:img src="img/logo_top.gif" width="257" height="75"/></td>
    <td vAlign=top height="87" width="102">&nbsp;</td>
    <td vAlign=top height="87" width="412" background="img/logo.jpg"> 
      <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="6"><b>CBS REPORT SERVER</b></font></div>
    </td>
  </tr>
  <tr> 
    <td vAlign=top colSpan=3 bgcolor="#001e94" align="left">
      <div align="right">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="30">&nbsp;</td>
            <html:form action="ReportList.do" method="POST" styleId="0">
            <td width="130" height="7" valign="top">
              <html:hidden property="cmd" value="1"/>
              <html:hidden property="freq" value="D"/>
              <html:image src="img/daily_b.gif" value="Submit" border="0"/>
            </td>
            </html:form>
            <html:form action="ReportList.do" method="POST" styleId="1">
            <td width="150" height="7" valign="top">
              <html:hidden property="cmd" value="1"/>
              <html:hidden property="freq" value="M"/>
              <html:image src="img/monthly_b.gif" value="Submit" border="0"/>
            </td>
            </html:form>
            <html:form action="ReportList.do" method="POST" styleId="2">
            <td width="180" height="7" valign="top">
              <html:img src="img/on_request_y.gif"/>
            </td>
            </html:form>
            <html:form action="ArchiveList.do" method="POST" styleId="3">
            <td height="7" align="right" valign="top" colSpan=2>
              <html:hidden property="cmd" value="1"/>
              <html:image src="img/request_old.gif" value="Submit" border="0"/>
            </td>
            </html:form>
          </tr>
          <tr height="5"><td></td></tr>
          <tr>
            <td colSpan=4></td>
            <td align="right" valign="top">
              <font face="MS Sans Serif, Microsoft Sans Serif" size="1" color="#FFFFFF"><b>Post Date &nbsp;</b></font>
            </td>
            <html:form action="ReportList.do" method="POST" styleId="4">
            <td width="10%" height="7" align="right" vAlign="top">
              <html:select property="postdate" size="1" onchange="document.forms[4].submit()">
                <html:options collection="dateBeanV" property="value" labelProperty="label"/>
              </html:select>
            </td>
            </html:form>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  </tbody> 
</table>

<br>
<table width="771" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="200" height="60" align="left" valign="top"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font face="MS Sans Serif, Microsoft Sans Serif" size="2">
          Branch Code : <jsp:getProperty name="loginBean" property="brcd"/></b></font></td>
        </tr>
        <tr>
          <td><font face="MS Sans Serif, Microsoft Sans Serif" size="2">
          <jsp:getProperty name="loginBean" property="brName"/></b></font></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
          <td><font face="MS Sans Serif, Microsoft Sans Serif" size="2">
          User ID : <jsp:getProperty name="loginBean" property="uid"/></font></td>
        </tr>
        <tr>
          <td><font face="MS Sans Serif, Microsoft Sans Serif" size="2">
          <jsp:getProperty name="loginBean" property="name"/></font></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr> 
          <td><font face="MS Sans Serif, Microsoft Sans Serif" size="2">
          Class : <jsp:getProperty name="loginBean" property="UClass"/></font></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <html:form action="ReportList.do" method="POST" styleId="5">
          <tr>
            <td>
            <font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>View Branch</b></font><br>
            <html:select property="brcd" size="1" onchange="document.forms[5].submit()">
              <html:options collection="brcdBeanV" property="value" labelProperty="label"/>
            </html:select>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>
            <font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>Report Class</b></font><br>
            <html:select property="repclass" size="1" onchange="document.forms[5].submit()">
              <html:options collection="repClassV" property="value" labelProperty="label"/>
            </html:select>
            </td>
          </tr>
        </html:form>
        <tr height="50"><td></td></tr>
        <tr>
          <td><html:link action="Logout.do"><html:img src="img/logout.gif" width="90" height="25" border="0"/></html:link></td>
        </tr>
      </table>
    </td>
    <td width="600" align="left" valign="top" height="60"> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr><td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#001e94">
              <td width="20%" align="left"><font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>| Report Code</b></font></td>
              <td width="15%" align="left"><font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>| Report Date</b></font></td>
              <td width="12%" align="left"><font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>| Size</font></b></td>
              <td width="53%" align="left"><font face="MS Sans Serif, Microsoft Sans Serif" size="1"><b>| Report Name</b></font></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <html:errors/>
            <logic:iterate id="repfile" name="repFileV" indexId="index">
              <tr onmouseover= "this.style.backgroundColor = '#556CBF';" onmouseout="this.style.backgroundColor = '#1E3DAA';" >
                <logic:equal name="repfile" property="status" value="0">
                  <td align="left" valign="top"><font color="#888888" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="code"/></font></td>
                  <td align="center" valign="top" colSpan=2><font color="#888888" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    -- n/a --</font></td>
                  <td align="left" valign="top"><font color="#888888" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="name"/></font></td>
                </logic:equal>
                <logic:equal name="repfile" property="status" value="1">
                  <td align="left" valign="top"><font color="#CCCCCC" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="code"/></font></td>
                  <td align="center" valign="top" colSpan=2><font color="#CCCCCC" face="MS Sans Serif, Microsoft Sans Serif" size="1">
                    [ no report ]</font></td>
                  <td align="left" valign="top"><font color="#CCCCCC" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="name"/></font></td>
                </logic:equal>
                <logic:equal name="repfile" property="status" value="2">
                  <bean:define id="code"    name="repfile" property="code"/>
                  <bean:define id="repdate" name="repfile" property="repDate"/>
                  <%
                    Hashtable downloadParam = new Hashtable();
                    downloadParam.put("code", code); 
                    downloadParam.put("freq", reportListBean.getFreq());
                    downloadParam.put("brcd", reportListBean.getBrcd());
                    downloadParam.put("repdate", repdate);
                    pageContext.setAttribute("downloadParam", downloadParam, PageContext.PAGE_SCOPE);
                  %>
                  <td align="left" valign="top"><font color="#FF9933" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="code"/></font></td>
                  <td align="left" valign="top"><font color="#FF9933" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="repDate"/></font></td>
                  <td align="left" valign="top"><font color="#FF9933" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <bean:write name="repfile" property="size"/></font></td>
                  <td align="left" valign="top"><font color="#FF9933" face="MS Sans Serif, Microsoft Sans Serif" size="2">
                    <html:link action="Download.do" name="downloadParam">
                    <bean:write name="repfile" property="name"/></html:link></font></td>
                </logic:equal>
              </tr>
            </logic:iterate>
          </table>
        </td></tr>
        <tr><td height="30">&nbsp;</td></tr>
      </table>
    </td>
  </tr>
</table>

<jsp:include page='page_footer.html'/>

</body>

</html:html>
