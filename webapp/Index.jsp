<%@ page contentType="text/html; charset=TIS-620" %>
<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<jsp:useBean id="indexBean" scope="request" type="com.tnis.reportapp.IndexBean"/>

<html:html>

<head>
<jsp:include page='html_head.html'/>
</head>

<body text=#FFFFFF bgColor=1E3DAA leftMargin=12 topMargin=12 marginHeight="0" marginWidth="0"
 link="#FF9933" vlink="#6699FF" alink="#6699FF">

<table cellSpacing=0 cellPadding=0 height=94 width=771 border=0>
  <tbody> 
  <tr> 
    <td vAlign=top height="87" width="257"><img src="img/logo_top.gif" width="257" height="75"></td>
    <td vAlign=top height="87" width="102">&nbsp;</td>
    <td vAlign=top height="87" width="412" background="img/logo.jpg"> 
      <div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="6"><b>CBS REPORT SERVER</b></font></div>
    </td>
  </tr>
  <tr> 
    <td vAlign=top width=257 bgcolor="#001e94" height="2" align="left">&nbsp;</td>
    <td vAlign=top colSpan=3 bgcolor="#001e94" height="2" align="left">&nbsp;</td>
  </tr>
  </tbody> 
</table>

<br>
<table cellspacing=0 cellpadding=0 width=771 border=0>
  <tbody> 
  <tr> 
    <td valign=top height=100 width=150> 
      <form name="loginForm" method="POST" action="<%= indexBean.getLoginURL() %>">
      <table cellspacing=0 cellpadding=0 width=73 border=0>
        <tr>
          <td valign=bottom colspan=2 height=5>
            <span class=tentext><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Login :</font></span>
          </td>
        </tr>
        <tr>
          <td colspan=2 height=11>
            <input type="text" name="uid" size="12" value="" style="WIDTH: 136px" class="fields">
          </td>
        </tr>

        <tr>
          <td valign=bottom colspan=2 height=7>
            <span class=tentext><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Password :</font></span>
          </td>
        </tr>
        <tr>
          <td width=125> 
            <input type="password" name="passwd" size="9" value="" style="WIDTH: 110px" class="fields">
          </td>
          <td width="32" align="right" valign="bottom"> 
            &nbsp;<input type="image" name="" src="img/button_ok.gif" align="bottom" border="0" value="Submit" alt="ok">
          </td>
        </tr>

      </table>
      </form>
    </td>
    <td>
      <center>
        <p>
        <font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">
        <jsp:getProperty name="indexBean" property="indexMsg1"/><br>
        <jsp:getProperty name="indexBean" property="indexMsg2"/><br>
        <jsp:getProperty name="indexBean" property="indexMsg3"/>
        </font>
        <p>
        <html:errors/>
      </center>
    </td>
  </tr>
  <tr> 
    <td valign=top height="2" align="left">&nbsp;</td>
    <td valign=top height="2" align="left">&nbsp;</td>
  </tr>
  </tbody> 
</table>

<jsp:include page='page_footer.html'/>

</body>

</html:html>
